﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Autonima;
namespace FiniteAtomina
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ParseInput_Click(object sender, EventArgs e)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                foreach (char c in InputBox.Text)
                {
                    ms.WriteByte((byte)c);
                }
                ms.Position = 0;

                StreamReader sr = new StreamReader(ms);

                fa = FiniteAutomata.Read(sr);

                Byte[] bytes = FiniteAutomata.GetImage(fa);

                Image i = Image.FromStream(new MemoryStream(bytes));
                pictureBox1.Image = i;



                ms = new MemoryStream();
                StreamWriter sw = new StreamWriter(ms);

                FiniteAutomata.Write(fa, sw);

                sw.Flush();
                ms.Position = 0;
                sr = new StreamReader(ms);

                string s = sr.ReadToEnd();

                OutputBox.Text = s+ "\r\n\r\n Fully Parsed Good Input!";
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                while (ex != null)
                {
                    sb.AppendLine(ex.Message);
                    ex = ex.InnerException;
                }

                OutputBox.Text = sb.ToString();
            }
        }

        FiniteAutomata fa = null;


        private void TestString_Click(object sender, EventArgs e)
        {
            string text = TestStringBox.Text;
            string output = "";
            bool result = false;

            switch (FiniteAutomataTab.SelectedIndex)
            {
                case 0:
                    result = FiniteAutomata.ParseString(fa, text, out output);
                    break;
                case 1:
                    result = PushDownAutomata.ParseString(PDA, text, out output);
                    break;
                case 2:
                    break;
                case 3:
                    result = CSA.TestString(text, out output);
                    break;
            }

            OutputBox.Text = output;
            OutputBox.Select(OutputBox.Text.Length - 2, 0);
            OutputBox.ScrollToCaret();

            label1.Text = "String Is In The Language: " + result.ToString();


        }

        private void LoadDFA_Click(object sender, EventArgs e)
        {
            InputBox.Text = DFA;
            TestStringBox.Text = "if:then:else";
            ParseInput_Click(sender, e);
        }

        private void LoadANFA_Click(object sender, EventArgs e)
        {
            InputBox.Text = NFA;
            TestStringBox.Text = "if:then:else";
            ParseInput_Click(sender, e);
        }


        string DFA = 
@"DeterministicFiniteAutomata
IfThenElse
A
A:B:C:D
if:then:else
A:C
A:if->B
A:then->D
A:else->D
B:then->C
B:if->D
B:else->D
C:else->A
C:if->B
C:then->D
D:if->D
D:then->D
D:else->D";
        string NFA =
@"NonDeterministicFiniteAutomata
IfThenElse
A
A:B:C
if:then:else:
A
A:if->B
B:then->C
C:else->A
C:->A";
        string NPDA =
  @"NonDeterministicPushDownAutomata
L={ww^R|w\\in{0,1}*}
Start
Start:q0:q1:q2
0:1:
$:a:b:
q2
Start:,->$;q0
q0:0,->a;q0
q0:1,->b;q0
q0:,->;q1
q1:0,a->;q1
q1:1,b->;q1
q1:,$->;q2";

        string NPDA2 =
@"NonDeterministicPushDownAutomata
L={aa^Nb^N}
qStart
qStart:qLoop:qAccept
a:b:
$:S:T:a:b:
qAccept
qStart:,->S.$;qLoop
qLoop:,S->a.T.b;qLoop
qLoop:,T->T.a;qLoop
qLoop:,S->b;qLoop
qLoop:,T->;qLoop
qLoop:a,a->;qLoop
qLoop:b,b->;qLoop
qLoop:,$->;qAccept";

        string AbuiguousGrammar =
@"NonDeterministicPushDownAutomata
Abiguous Grammar for Language: {a^nb^nc^md^m|m,n ≥ 1} ∪ {a^nb^mc^md^n^|m,n ≥ 1}
qStart
qStart:qLoop:qAccept
a:b:c:d:
$:S:S1:S2:A:B:C:a:b:c:d:
qAccept
qStart:,->S.$;qLoop
qLoop:,S->S1;qLoop
qLoop:,S->S2;qLoop
qLoop:,S1->A.B;qLoop
qLoop:,A->a.A.b;qLoop
qLoop:,A->a.b;qLoop
qLoop:,B->c.B.d;qLoop
qLoop:,B->c.d;qLoop
qLoop:,S2->a.S2.d;qLoop
qLoop:,S2->a.C.d;qLoop
qLoop:,C->b.C.c;qLoop
qLoop:,C->b.c;qLoop
qLoop:a,a->;qLoop
qLoop:b,b->;qLoop
qLoop:c,c->;qLoop
qLoop:d,d->;qLoop
qLoop:,$->;qAccept";

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CIS 520 (*.520)|*.520";
            if(ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                String s = File.ReadAllText(ofd.FileName);
                InputBox.Text = s;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JPEG Images (*.jpg)|*.JPG";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = sfd.FileName;
                pictureBox1.Image.Save(filename);

            }
        }

        private void SaveFiniteAutomata_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CIS 520 (*.520)|*.520";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, InputBox.Text);
            }
        }

        PushDownAutomata PDA = null;
        private void ParsePDA_Click(object sender, EventArgs e)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                foreach (char c in InputBox.Text)
                {
                    ms.WriteByte((byte)c);
                }
                ms.Position = 0;

                StreamReader sr = new StreamReader(ms);

                PDA = PushDownAutomata.Read(sr);


               
                Byte[] bytes = PushDownAutomata.GetImage(PDA);

                Image i = Image.FromStream(new MemoryStream(bytes));
                pictureBox1.Image = i;



                ms = new MemoryStream();
                StreamWriter sw = new StreamWriter(ms);

                PushDownAutomata.Write(PDA, sw);
                sw.Flush();
                ms.Position = 0;
                sr = new StreamReader(ms);

                string s = sr.ReadToEnd();

                OutputBox.Text = s + "\r\n\r\n Fully Parsed Good Input!";
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                while (ex != null)
                {
                    sb.AppendLine(ex.Message);
                    ex = ex.InnerException;
                }

                OutputBox.Text = sb.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InputBox.Text = NPDA;

            ParsePDA_Click(sender, e);

            TestStringBox.Text = "0101111010";

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            InputBox.Text = NPDA2;

            ParsePDA_Click(sender, e);

            TestStringBox.Text = "aaaaab";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            InputBox.Text = AbuiguousGrammar;
            ParsePDA_Click(sender, e);
            TestStringBox.Text = "aaabbbcccddd";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            InputBox.Text = Precidence;
            ParsePDA_Click(sender, e);
            TestStringBox.Text = "(a+a)";
        }

        string Precidence =
@"NonDeterministicPushDownAutomata
Precidence Operators (),*,+
qStart
qStart:qLoop:qAccept
a:+:*:(:):
$:E:T:F:(:):*:+:a:
qAccept
qStart:,->E.$;qLoop
qLoop:,E->E.+.T;qLoop
qLoop:,E->T;qLoop
qLoop:,T->T.*.F;qLoop
qLoop:,T->F;qLoop
qLoop:,F->(.E.);qLoop
qLoop:,F->a;qLoop
qLoop:(,(->;qLoop
qLoop:),)->;qLoop
qLoop:*,*->;qLoop
qLoop:+,+->;qLoop
qLoop:a,a->;qLoop
qLoop:,$->;qAccept";

        private void button6_Click(object sender, EventArgs e)
        {
            InputBox.Text = ifthenelse;
            ParsePDA_Click(sender, e);
            TestStringBox.Text = "if:e:then:other:else:if:e:then:other";
        }

        string ifthenelse =
@"NonDeterministicPushDownAutomata
if E then S | if E then S else S
qStart
qStart:qLoop:qAccept
if:then:else:e:other:
$:S:S1:S2:E:if:then:else:e:other:
qAccept
qStart:,->S.$;qLoop
qLoop:,S->S1;qLoop
qLoop:,S->S2;qLoop
qLoop:,S1->if.E.then.S1.else.S1;qLoop
qLoop:,S1->other;qLoop
qLoop:,S2->if.E.then.S;qLoop
qLoop:,S2->if.E.then.S1.else.S2;qLoop
qLoop:,E->e;qLoop
qLoop:other,other->;qLoop
qLoop:if,if->;qLoop
qLoop:then,then->;qLoop
qLoop:else,else->;qLoop
qLoop:e,e->;qLoop
qLoop:,$->;qAccept";

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

            try
            {
                CSA = new ContextSensitiveAutomata(InputBox.Text);

                OutputBox.Text = CSA.ToString() + "\r\nFULLY PARSED GOOD INPUT!";

                Byte[] bytes = CSA.GetImage();

                Image i = Image.FromStream(new MemoryStream(bytes));
                pictureBox1.Image = i;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                while (ex != null)
                {
                    sb.AppendLine(ex.Message);
                    ex = ex.InnerException;
                }
                OutputBox.Text = sb.ToString();
            }
        }

        ContextSensitiveAutomata CSA = null;

        string abc_n =

@"ContextSensitiveAutomata
L = {a^n b^n c^n}
Start
(Start,Loop,NLoop,BA_AB,CA_AC,CB_BC,End)
(a,b,c)
($,S,a,b,c)
(End)
(State_N) -> (StateM) : (InputListMatch) -> (InputListAfterMatch) : (StackListMatch) -> (StackListAfterMatch)
(Start)	->	(Loop)	:	()	->	()	:	()	->	(S,$)
(Loop)	->	(Loop)	:	(a)	->	(a)	:	(S)	->	(a,b,c)
(Loop)	->	(Loop)	:	(a)	->	(a)	:	(S)	->	(a,S,b,c)
(Loop)	->	(BA_AB)	:	(a)	->	(a)	:	(b)	->	(b)
(BA_AB)	->	(Loop)	:	()	->	()	:	(b,a)	->	(a,b)
(Loop)	->	(CA_AC)	:	(a)	->	(a)	:	(c)	->	(c)
(CA_AC)	->	(Loop)	:	()	->	()	:	(c,a)	->	(a,c)
(Loop)	->	(CB_BC)	:	(b)	->	(b)	:	(c)	->	(c)
(CB_BC)	->	(Loop)	:	()	->	()	:	(c,b)	->	(b,c)
(Loop)	->	(Loop)	:	(a)	->	()	:	(a)	->	()
(Loop)	->	(Loop)	:	(b)	->	()	:	(b)	->	()
(Loop)	->	(Loop)	:	(c)	->	()	:	(c)	->	()
(Loop)	->	(NLoop)	:	(c)	->	()	:	(c,$)	->	($)
(NLoop)	->	(End)	:	()	->	()	:	($)	->	()
";

        private void button9_Click(object sender, EventArgs e)
        {
            InputBox.Text = abc_n;
            button8_Click(sender, e);

            TestStringBox.Text = "aaaaaaaaabbbbbbbbbccccccccc";
        }

    }
}
