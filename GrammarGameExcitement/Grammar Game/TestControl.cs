﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autonima;

namespace GrammarGame
{
    public partial class TestControl : UserControl
    {
        public TestControl()
        {
            InitializeComponent();
        }

        public void ClearTest()
        {
            colorList1.ClearColors();
        }

        public List<GrammarColor> GetTestColors()
        {
            return colorList1.GetListOfColors();
        }

        public void SetTestColorS(List<GrammarColor> colors)
        {
            colorList1.SetListOfColors(colors);
        }

        public void SetColorsUsed(List<GrammarColor> colors)
        {
            colorList1.SetColorsUsed(colors);
        }
    }
}
