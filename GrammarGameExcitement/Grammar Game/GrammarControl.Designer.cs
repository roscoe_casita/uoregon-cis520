﻿namespace GrammarGame
{
    partial class GrammarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrammarControl));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.S3 = new GrammarGame.ColorList();
            this.S2 = new GrammarGame.ColorList();
            this.S1 = new GrammarGame.ColorList();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.R3R = new GrammarGame.ColorList();
            this.R3S = new GrammarGame.ColorList();
            this.label2 = new System.Windows.Forms.Label();
            this.R2R = new GrammarGame.ColorList();
            this.R2S = new GrammarGame.ColorList();
            this.label1 = new System.Windows.Forms.Label();
            this.R1R = new GrammarGame.ColorList();
            this.R1S = new GrammarGame.ColorList();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(668, 284);
            this.splitContainer1.SplitterDistance = 193;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.S3);
            this.groupBox1.Controls.Add(this.S2);
            this.groupBox1.Controls.Add(this.S1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 284);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sentences";
            // 
            // S3
            // 
            this.S3.Location = new System.Drawing.Point(6, 185);
            this.S3.Name = "S3";
            this.S3.Size = new System.Drawing.Size(164, 78);
            this.S3.TabIndex = 2;
            this.S3.TokenCount = 2;
            // 
            // S2
            // 
            this.S2.Location = new System.Drawing.Point(6, 102);
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(164, 78);
            this.S2.TabIndex = 1;
            this.S2.TokenCount = 2;
            // 
            // S1
            // 
            this.S1.Location = new System.Drawing.Point(6, 19);
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(164, 78);
            this.S1.TabIndex = 0;
            this.S1.TokenCount = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.R3R);
            this.groupBox2.Controls.Add(this.R3S);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.R2R);
            this.groupBox2.Controls.Add(this.R2S);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.R1R);
            this.groupBox2.Controls.Add(this.R1S);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(471, 284);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rules";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "=>";
            // 
            // R3R
            // 
            this.R3R.Location = new System.Drawing.Point(217, 187);
            this.R3R.Name = "R3R";
            this.R3R.Size = new System.Drawing.Size(246, 78);
            this.R3R.TabIndex = 7;
            this.R3R.TokenCount = 3;
            // 
            // R3S
            // 
            this.R3S.Location = new System.Drawing.Point(6, 187);
            this.R3S.Name = "R3S";
            this.R3S.Size = new System.Drawing.Size(164, 78);
            this.R3S.TabIndex = 6;
            this.R3S.TokenCount = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "=>";
            // 
            // R2R
            // 
            this.R2R.Location = new System.Drawing.Point(218, 104);
            this.R2R.Name = "R2R";
            this.R2R.Size = new System.Drawing.Size(246, 78);
            this.R2R.TabIndex = 4;
            this.R2R.TokenCount = 3;
            // 
            // R2S
            // 
            this.R2S.Location = new System.Drawing.Point(7, 104);
            this.R2S.Name = "R2S";
            this.R2S.Size = new System.Drawing.Size(164, 78);
            this.R2S.TabIndex = 3;
            this.R2S.TokenCount = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(176, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "=>";
            // 
            // R1R
            // 
            this.R1R.Location = new System.Drawing.Point(217, 19);
            this.R1R.Name = "R1R";
            this.R1R.Size = new System.Drawing.Size(246, 78);
            this.R1R.TabIndex = 1;
            this.R1R.TokenCount = 3;
            // 
            // R1S
            // 
            this.R1S.Location = new System.Drawing.Point(6, 19);
            this.R1S.Name = "R1S";
            this.R1S.Size = new System.Drawing.Size(164, 78);
            this.R1S.TabIndex = 0;
            this.R1S.TokenCount = 2;
            // 
            // GrammarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "GrammarControl";
            this.Size = new System.Drawing.Size(668, 284);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private ColorList S3;
        private ColorList S2;
        private ColorList S1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private ColorList R3R;
        private ColorList R3S;
        private System.Windows.Forms.Label label2;
        private ColorList R2R;
        private ColorList R2S;
        private System.Windows.Forms.Label label1;
        private ColorList R1R;
        private ColorList R1S;
    }
}
