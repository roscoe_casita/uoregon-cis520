﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autonima;
namespace GrammarGame
{
    public partial class ColorList : UserControl
    {
        public ColorList()
        {
            InitializeComponent();
            while (flowLayoutPanel1.Controls.Count < _TokenCount)
            {
                ColorCircle cc = new ColorCircle();
                cc.CurrentColor = GrammarColor.None;
                flowLayoutPanel1.Controls.Add(cc);
            }
        }

        private int _TokenCount = 2;

        [PropertyTab("TokenCount")]
        [DisplayName("NumberOfTokens")]
        [Category("TokenList")]
        public int TokenCount
        {
            get
            {
                return _TokenCount;
            }
            set
            {
                if (_TokenCount != value)
                {
                    _TokenCount = value;
                    flowLayoutPanel1.Controls.Clear();
                    while (flowLayoutPanel1.Controls.Count < _TokenCount)
                    {
                        ColorCircle cc = new ColorCircle();
                        cc.CurrentColor = GrammarColor.None;
                        flowLayoutPanel1.Controls.Add(cc);
                    }
                }
            }
        }

        public void ClearColors()
        {
            flowLayoutPanel1.Controls.Clear();
            while (flowLayoutPanel1.Controls.Count < _TokenCount)
            {
                ColorCircle cc = new ColorCircle();
                cc.CurrentColor = GrammarColor.None;
                flowLayoutPanel1.Controls.Add(cc);
            }
        }


        public List<string> GetListOfColorsStrings()
        {
            return Helpers.Convert(GetListOfColors());
        }

        public void SetListOfColorsStrings(List<string> value)
        {
            SetListOfColors(Helpers.Convert(value));

        }

        public List<GrammarColor> GetListOfColors()
        {
            List<GrammarColor> returnValue = new List<GrammarColor>();
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                ColorCircle cc = c as ColorCircle;
                if (cc != null)
                {
                    returnValue.Add(cc.CurrentColor);
                }
            }
            return returnValue;
        }
        public void SetListOfColors(List<GrammarColor> value)
        {
            if (value.Count > _TokenCount)
                throw new Exception("Invalid Token Count.");
            flowLayoutPanel1.Controls.Clear();

            foreach (GrammarColor gc in value)
            {
                ColorCircle cc = new ColorCircle();
                cc.CurrentColor = gc;
                flowLayoutPanel1.Controls.Add(cc);
            }
            while (flowLayoutPanel1.Controls.Count < _TokenCount)
            {
                ColorCircle cc = new ColorCircle();
                cc.CurrentColor = GrammarColor.None;
                flowLayoutPanel1.Controls.Add(cc);
            }
        }

        public int RealTokenCount
        {
            get
            {
                int returnValue = 0;
                foreach (Control c in flowLayoutPanel1.Controls)
                {
                    ColorCircle cc = c as ColorCircle;

                    if (cc.CurrentColor.CompareTo(GrammarColor.None) != 0)
                    {
                        returnValue++;
                    }

                }
                return returnValue;
            }
        }

        public List<GrammarColor> GetColorsUsed()
        {
            List<GrammarColor> Colors = new List<GrammarColor>();
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                ColorCircle cc = c as ColorCircle;
                if (cc.CurrentColor.CompareTo(GrammarColor.None) != 0)
                {
                    if (!Colors.Contains(cc.CurrentColor))
                    {
                        Colors.Add(cc.CurrentColor);
                    }
                }
            }
            return Colors;
        }

        public void SetColorsUsed(List<GrammarColor> value)
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                ColorCircle cc = c as ColorCircle;
                cc.SetDict(value);
            }
        }
    }
}
