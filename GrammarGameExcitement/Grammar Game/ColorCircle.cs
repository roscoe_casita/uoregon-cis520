﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autonima;

namespace GrammarGame
{
  
    public partial class ColorCircle : UserControl
    {
       

        public void SetDict(List<GrammarColor> ColorTransitions)
        {
            ColorMoves.Clear();

            GrammarColor from = GrammarColor.None;
            GrammarColor to = GrammarColor.None;
            for(int i = 0; i< ColorTransitions.Count; i ++)
            {
                to = ColorTransitions[i];
                ColorMoves.Add(from, to);
                from = to;
            }
            to = GrammarColor.None;
            ColorMoves.Add(from, to);

        }

        Dictionary<GrammarColor, GrammarColor> ColorMoves = new Dictionary<GrammarColor,GrammarColor>();

        
       
        public ColorCircle()
        {
            InitializeComponent();
            List<GrammarColor> Standard = new List<GrammarColor>();
            Standard.Add(GrammarColor.Red);
            Standard.Add(GrammarColor.Blue);
            Standard.Add(GrammarColor.Green);
            Standard.Add(GrammarColor.Yellow);
            SetDict(Standard);
        }

        private void ColorCircle_Load(object sender, EventArgs e)
        {

        }

        private void ColorCircle_Click(object sender, EventArgs e)
        {
            CurrentColor = ColorMoves[CurrentColor];
        }

        public GrammarColor CurrentColor
        {
            get
            {
                return Color;
            }
            set
            {
                Color = value;
                this.Invalidate();
            }
        }

        GrammarColor Color = GrammarColor.None;

        private void ColorCircle_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(new Point(0, 0), this.Size);
            r.Width -= 1;
            r.Height -= 1;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            e.Graphics.Clear(this.BackColor);
            e.Graphics.DrawRectangle(System.Drawing.Pens.Black, r);




            switch (CurrentColor)
            {
                case GrammarColor.None:
                    break;

                case GrammarColor.Red:
                    e.Graphics.FillEllipse(System.Drawing.Brushes.Red,r);
                    break;

                case GrammarColor.Blue:
                    e.Graphics.FillEllipse(System.Drawing.Brushes.Blue, r);
                    break;

                case GrammarColor.Green:
                    e.Graphics.FillEllipse(System.Drawing.Brushes.Green, r);
                    break;
                case GrammarColor.Yellow:
                    e.Graphics.FillEllipse(System.Drawing.Brushes.Yellow, r);
                    break;
            }
        }

        private void ColorCircle_DoubleClick(object sender, EventArgs e)
        {
            ColorCircle_Click(sender, e);
        }
    }

}
