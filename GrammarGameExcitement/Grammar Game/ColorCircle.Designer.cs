﻿namespace GrammarGame
{
    partial class ColorCircle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ColorCircle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.DoubleBuffered = true;
            this.Name = "ColorCircle";
            this.Size = new System.Drawing.Size(75, 75);
            this.Load += new System.EventHandler(this.ColorCircle_Load);
            this.Click += new System.EventHandler(this.ColorCircle_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ColorCircle_Paint);
            this.DoubleClick += new System.EventHandler(this.ColorCircle_DoubleClick);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
