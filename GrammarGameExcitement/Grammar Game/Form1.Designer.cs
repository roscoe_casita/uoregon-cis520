﻿using Autonima;
namespace GrammarGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.rulesTab = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grammarControl1 = new GrammarGame.GrammarControl();
            this.ClearRules = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.gameTab = new System.Windows.Forms.TabPage();
            this.ColorsUsedLabel = new System.Windows.Forms.Label();
            this.TokenCountLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.GameResults = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TestResults = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TestAGuess = new System.Windows.Forms.Button();
            this.ClearGrammar = new System.Windows.Forms.Button();
            this.GuessGrammar = new System.Windows.Forms.Button();
            this.testControl1 = new GrammarGame.TestControl();
            this.grammarControl2 = new GrammarGame.GrammarControl();
            this.historyTab = new System.Windows.Forms.TabPage();
            this.consoleTab = new System.Windows.Forms.TabPage();
            this.OutputDump = new System.Windows.Forms.RichTextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GoodGuessesPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BadGuessesPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabControl1.SuspendLayout();
            this.rulesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gameTab.SuspendLayout();
            this.historyTab.SuspendLayout();
            this.consoleTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.rulesTab);
            this.tabControl1.Controls.Add(this.gameTab);
            this.tabControl1.Controls.Add(this.historyTab);
            this.tabControl1.Controls.Add(this.consoleTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1397, 770);
            this.tabControl1.TabIndex = 5;
            // 
            // rulesTab
            // 
            this.rulesTab.Controls.Add(this.splitContainer1);
            this.rulesTab.Location = new System.Drawing.Point(4, 22);
            this.rulesTab.Name = "rulesTab";
            this.rulesTab.Padding = new System.Windows.Forms.Padding(3);
            this.rulesTab.Size = new System.Drawing.Size(1389, 744);
            this.rulesTab.TabIndex = 0;
            this.rulesTab.Text = "Rules";
            this.rulesTab.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grammarControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ClearRules);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Size = new System.Drawing.Size(1383, 738);
            this.splitContainer1.SplitterDistance = 296;
            this.splitContainer1.TabIndex = 0;
            // 
            // grammarControl1
            // 
            this.grammarControl1.BackColor = System.Drawing.Color.White;
            this.grammarControl1.Location = new System.Drawing.Point(5, 3);
            this.grammarControl1.Name = "grammarControl1";
            this.grammarControl1.Size = new System.Drawing.Size(668, 284);
            this.grammarControl1.TabIndex = 0;
            // 
            // ClearRules
            // 
            this.ClearRules.Location = new System.Drawing.Point(5, 53);
            this.ClearRules.Name = "ClearRules";
            this.ClearRules.Size = new System.Drawing.Size(174, 23);
            this.ClearRules.TabIndex = 2;
            this.ClearRules.Text = "Clear Rules";
            this.ClearRules.UseVisualStyleBackColor = true;
            this.ClearRules.Click += new System.EventHandler(this.ClearRules_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(205, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(174, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Make Rules && Play a Game";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gameTab
            // 
            this.gameTab.Controls.Add(this.ColorsUsedLabel);
            this.gameTab.Controls.Add(this.TokenCountLabel);
            this.gameTab.Controls.Add(this.label5);
            this.gameTab.Controls.Add(this.label4);
            this.gameTab.Controls.Add(this.GameResults);
            this.gameTab.Controls.Add(this.label3);
            this.gameTab.Controls.Add(this.TestResults);
            this.gameTab.Controls.Add(this.label2);
            this.gameTab.Controls.Add(this.TestAGuess);
            this.gameTab.Controls.Add(this.ClearGrammar);
            this.gameTab.Controls.Add(this.GuessGrammar);
            this.gameTab.Controls.Add(this.testControl1);
            this.gameTab.Controls.Add(this.grammarControl2);
            this.gameTab.Location = new System.Drawing.Point(4, 22);
            this.gameTab.Name = "gameTab";
            this.gameTab.Padding = new System.Windows.Forms.Padding(3);
            this.gameTab.Size = new System.Drawing.Size(1389, 744);
            this.gameTab.TabIndex = 1;
            this.gameTab.Text = "Game";
            this.gameTab.UseVisualStyleBackColor = true;
            // 
            // ColorsUsedLabel
            // 
            this.ColorsUsedLabel.AutoSize = true;
            this.ColorsUsedLabel.Location = new System.Drawing.Point(776, 69);
            this.ColorsUsedLabel.Name = "ColorsUsedLabel";
            this.ColorsUsedLabel.Size = new System.Drawing.Size(0, 13);
            this.ColorsUsedLabel.TabIndex = 12;
            // 
            // TokenCountLabel
            // 
            this.TokenCountLabel.AutoSize = true;
            this.TokenCountLabel.Location = new System.Drawing.Point(781, 30);
            this.TokenCountLabel.Name = "TokenCountLabel";
            this.TokenCountLabel.Size = new System.Drawing.Size(0, 13);
            this.TokenCountLabel.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(703, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Colors Used:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(703, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Token Count:";
            // 
            // GameResults
            // 
            this.GameResults.AutoSize = true;
            this.GameResults.Location = new System.Drawing.Point(778, 222);
            this.GameResults.Name = "GameResults";
            this.GameResults.Size = new System.Drawing.Size(0, 13);
            this.GameResults.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(703, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Game Results:";
            // 
            // TestResults
            // 
            this.TestResults.AutoSize = true;
            this.TestResults.Location = new System.Drawing.Point(778, 346);
            this.TestResults.Name = "TestResults";
            this.TestResults.Size = new System.Drawing.Size(0, 13);
            this.TestResults.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(703, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Test Results:";
            // 
            // TestAGuess
            // 
            this.TestAGuess.Location = new System.Drawing.Point(703, 296);
            this.TestAGuess.Name = "TestAGuess";
            this.TestAGuess.Size = new System.Drawing.Size(183, 23);
            this.TestAGuess.TabIndex = 4;
            this.TestAGuess.Text = "Test A Guess";
            this.TestAGuess.UseVisualStyleBackColor = true;
            this.TestAGuess.Click += new System.EventHandler(this.TestAGuess_Click);
            // 
            // ClearGrammar
            // 
            this.ClearGrammar.Location = new System.Drawing.Point(703, 126);
            this.ClearGrammar.Name = "ClearGrammar";
            this.ClearGrammar.Size = new System.Drawing.Size(183, 23);
            this.ClearGrammar.TabIndex = 3;
            this.ClearGrammar.Text = "Clear Grammar";
            this.ClearGrammar.UseVisualStyleBackColor = true;
            this.ClearGrammar.Click += new System.EventHandler(this.ClearGrammar_Click);
            // 
            // GuessGrammar
            // 
            this.GuessGrammar.Location = new System.Drawing.Point(703, 169);
            this.GuessGrammar.Name = "GuessGrammar";
            this.GuessGrammar.Size = new System.Drawing.Size(183, 23);
            this.GuessGrammar.TabIndex = 2;
            this.GuessGrammar.Text = "Guess The Grammar";
            this.GuessGrammar.UseVisualStyleBackColor = true;
            this.GuessGrammar.Click += new System.EventHandler(this.GuessGrammar_Click);
            // 
            // testControl1
            // 
            this.testControl1.BackColor = System.Drawing.Color.White;
            this.testControl1.Location = new System.Drawing.Point(8, 296);
            this.testControl1.Name = "testControl1";
            this.testControl1.Size = new System.Drawing.Size(666, 87);
            this.testControl1.TabIndex = 1;
            // 
            // grammarControl2
            // 
            this.grammarControl2.BackColor = System.Drawing.Color.White;
            this.grammarControl2.Location = new System.Drawing.Point(6, 6);
            this.grammarControl2.Name = "grammarControl2";
            this.grammarControl2.Size = new System.Drawing.Size(668, 284);
            this.grammarControl2.TabIndex = 0;
            // 
            // historyTab
            // 
            this.historyTab.Controls.Add(this.splitContainer2);
            this.historyTab.Location = new System.Drawing.Point(4, 22);
            this.historyTab.Name = "historyTab";
            this.historyTab.Padding = new System.Windows.Forms.Padding(3);
            this.historyTab.Size = new System.Drawing.Size(1389, 744);
            this.historyTab.TabIndex = 3;
            this.historyTab.Text = "History";
            this.historyTab.UseVisualStyleBackColor = true;
            // 
            // consoleTab
            // 
            this.consoleTab.Controls.Add(this.OutputDump);
            this.consoleTab.Location = new System.Drawing.Point(4, 22);
            this.consoleTab.Name = "consoleTab";
            this.consoleTab.Padding = new System.Windows.Forms.Padding(3);
            this.consoleTab.Size = new System.Drawing.Size(1389, 744);
            this.consoleTab.TabIndex = 4;
            this.consoleTab.Text = "Console";
            this.consoleTab.UseVisualStyleBackColor = true;
            // 
            // OutputDump
            // 
            this.OutputDump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputDump.Location = new System.Drawing.Point(3, 3);
            this.OutputDump.Name = "OutputDump";
            this.OutputDump.Size = new System.Drawing.Size(1383, 738);
            this.OutputDump.TabIndex = 0;
            this.OutputDump.Text = "";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(1383, 738);
            this.splitContainer2.SplitterDistance = 678;
            this.splitContainer2.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GoodGuessesPanel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(678, 738);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Good Guesses";
            // 
            // GoodGuessesPanel
            // 
            this.GoodGuessesPanel.AutoScroll = true;
            this.GoodGuessesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GoodGuessesPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.GoodGuessesPanel.Location = new System.Drawing.Point(3, 16);
            this.GoodGuessesPanel.Name = "GoodGuessesPanel";
            this.GoodGuessesPanel.Size = new System.Drawing.Size(672, 719);
            this.GoodGuessesPanel.TabIndex = 0;
            this.GoodGuessesPanel.WrapContents = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BadGuessesPanel);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(701, 738);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bad Guesses";
            // 
            // BadGuessesPanel
            // 
            this.BadGuessesPanel.AutoScroll = true;
            this.BadGuessesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BadGuessesPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.BadGuessesPanel.Location = new System.Drawing.Point(3, 16);
            this.BadGuessesPanel.Name = "BadGuessesPanel";
            this.BadGuessesPanel.Size = new System.Drawing.Size(695, 719);
            this.BadGuessesPanel.TabIndex = 0;
            this.BadGuessesPanel.WrapContents = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1397, 770);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.rulesTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gameTab.ResumeLayout(false);
            this.gameTab.PerformLayout();
            this.historyTab.ResumeLayout(false);
            this.consoleTab.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage rulesTab;
        private System.Windows.Forms.TabPage gameTab;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private GrammarControl grammarControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ClearRules;
        private GrammarControl grammarControl2;
        private System.Windows.Forms.TabPage historyTab;
        private System.Windows.Forms.TabPage consoleTab;
        private System.Windows.Forms.RichTextBox OutputDump;
        private System.Windows.Forms.Label TestResults;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button TestAGuess;
        private System.Windows.Forms.Button ClearGrammar;
        private System.Windows.Forms.Button GuessGrammar;
        private TestControl testControl1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label GameResults;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TokenCountLabel;
        private System.Windows.Forms.Label ColorsUsedLabel;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel GoodGuessesPanel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel BadGuessesPanel;
    }
}

