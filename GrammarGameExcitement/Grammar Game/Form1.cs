﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autonima;

namespace GrammarGame
{


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.DoubleBuffer |  ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            label1.Text = "No Rules Loaded.";

            RemPage(gameTab);
            RemPage(historyTab);
            RulesMode();

        }


        ContextSensitiveAutomata CSA = null;


        private void button1_Click(object sender, EventArgs e)
        {


            GameRules gr = grammarControl1.GetGameRules();

            string s = gr.GenerateMachineRules();

            try
            {
                CSA = new ContextSensitiveAutomata(s);
                label1.Text = "Made Rules!";
                OutputDump.Text = CSA.ToString();

                TokenCountLabel.Text = grammarControl1.TokenCount.ToString();

                List<GrammarColor> colors = grammarControl1.GetColorsUsed();

                ColorsUsedLabel.Text = Helpers.GenerateListItems(Helpers.Convert(colors));

                grammarControl2.SetColorsUsed(colors);

                testControl1.SetColorsUsed(colors);

                GameMode();

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                while (ex != null)
                {
                    sb.AppendLine(ex.Message);
                    ex = ex.InnerException;
                }

                MessageBox.Show(sb.ToString(), "Unable to Create Game!", MessageBoxButtons.OK);

                tabControl1.SelectedIndex = 2;
                label1.Text = sb.ToString();
            }
        }

        private void ClearRules_Click(object sender, EventArgs e)
        {
            grammarControl1.ClearGameRules();
            CSA = null;
            OutputDump.Text = "";
            label1.Text = "No Rules Loaded.";
            RemPage(gameTab);
            RemPage(historyTab);
        }

        private void TestAGuess_Click(object sender, EventArgs e)
        {
            List<GrammarColor> colors = testControl1.GetTestColors();
            List<string> test = Helpers.Convert(colors);
            test.Add("$");

            if (CSA != null)
            {
                string sb;
                bool result = CSA.BetterTest(test, out sb);

                OutputDump.Text = sb;
                OutputDump.Select(OutputDump.Text.Length - 2, 0);
                OutputDump.ScrollToCaret();

                TestControl tc = new TestControl();
                tc.SetTestColorS(colors);

                tc.BackColor = Color.White;

                if (result)
                {
                   TestResults.Text = "Sequence is in the language.";
                   GoodGuessesPanel.Controls.Add(tc);

                }
                else
                {
                    TestResults.Text = "Sequence is NOT in the language.";
                    BadGuessesPanel.Controls.Add(tc);
                }

            }
            else
            {
                MessageBox.Show("Please Enter Rules First.", "Please Enter Rules First.", MessageBoxButtons.OK);
            }
        }


        private void ClearGrammar_Click(object sender, EventArgs e)
        {
            grammarControl2.ClearGameRules();
        }

        void AddPage(TabPage tp)
        {
            if (!tabControl1.TabPages.Contains(tp))
            {
                tabControl1.TabPages.Add(tp);
            }
        }
        void RemPage(TabPage tp)
        {
            if (tabControl1.TabPages.Contains(tp))
            {
                tabControl1.TabPages.Remove(tp);
            }
        }
        void GameMode()
        {
            RemPage(rulesTab);
            RemPage(consoleTab);
            AddPage(gameTab);
            AddPage(historyTab);
        }

        void RulesMode()
        {
            AddPage(rulesTab);
            AddPage(consoleTab);
        }
        private void GuessGrammar_Click(object sender, EventArgs e)
        {
            GameRules gr1 = grammarControl1.GetGameRules();
            GameRules gr2 = grammarControl2.GetGameRules();

            bool Success = gr1.CompareGameSets(gr2);
            if (Success)
            {
                MessageBox.Show("You have picked wisely.", "You have picked wisely.", MessageBoxButtons.OK);
                GameResults.Text = "You have picked wisely.";
                RulesMode();
            }
            else
            {
                GameResults.Text = "You did not pick wisely.";
            }
        }
    } 
    
    public class GameRules
    {
        public List<GrammarColor> Sentance1 = new List<GrammarColor>();
        public List<GrammarColor> Sentance2 = new List<GrammarColor>();
        public List<GrammarColor> Sentance3 = new List<GrammarColor>();


        public List<GrammarColor> RuleMatch1 = new List<GrammarColor>();
        public List<GrammarColor> RuleMatch2 = new List<GrammarColor>();
        public List<GrammarColor> RuleMatch3 = new List<GrammarColor>();

        public List<GrammarColor> RuleReplacement1 = new List<GrammarColor>();
        public List<GrammarColor> RuleReplacement2 = new List<GrammarColor>();
        public List<GrammarColor> RuleReplacement3 = new List<GrammarColor>();


        public bool CompareGameSets(GameRules gr)
        {
            if (Helpers.CompareMultiLists(Sentances(), gr.Sentances()))
            {
                if (Helpers.CompareMultiLists(Rules(), gr.Rules()))
                {
                    return true;
                }
            }
            return false;
        }

        private List<List<string>> Rules()
        {
            List<List<string>> returnValue = new List<List<string>>();

            List<string> temp = Helpers.Convert(RuleMatch1);
            temp.AddRange(Helpers.Convert(RuleReplacement1));
            returnValue.Add(temp);
            temp = Helpers.Convert(RuleMatch2);
            temp.AddRange(Helpers.Convert(RuleReplacement2));
            returnValue.Add(temp);
            temp = Helpers.Convert(RuleMatch3);
            temp.AddRange(Helpers.Convert(RuleReplacement3));
            returnValue.Add(temp);
            return returnValue;
        }


        private List<List<string>> Sentances()
        {
            List<List<string>> returnValue = new List<List<string>>();

            returnValue.Add(Helpers.Convert(Sentance1));
            returnValue.Add(Helpers.Convert(Sentance2));
            returnValue.Add(Helpers.Convert(Sentance3));
            return returnValue;
        }

        void GenerateSentanceRule(List<GrammarColor> list, StringBuilder sb)
        {
            bool EmptyList = true;

            foreach (GrammarColor gc in list)
            {
                if (gc.CompareTo(GrammarColor.None) != 0)
                {
                    EmptyList = false;
                    break;
                }
            }
            if (EmptyList == true)
            {
                return;
            }
            List<string> statement = Helpers.Convert(list);
            statement.Add("$");
            sb.AppendLine(String.Format("(Loop)\t->\t(Loop)\t:\t{0}\t->\t($)\t:\t($)->($)", Helpers.GenerateListItems(statement)));

        }
        
        private void GenerateRuleStatement(List<GrammarColor> RuleMatch1, List<GrammarColor> RuleReplacement1, StringBuilder sb)
        {
            bool EmptyList = true;

            foreach (GrammarColor gc in RuleMatch1)
            {
                if (gc.CompareTo(GrammarColor.None) != 0)
                {
                    EmptyList = false;
                    break;
                }
            }
            foreach (GrammarColor gc in RuleReplacement1)
            {
                if (gc.CompareTo(GrammarColor.None) != 0)
                {
                    EmptyList = false;
                    break;
                }
            }    
            if (EmptyList == true)
            {
                return;
            }
            List<string> match = Helpers.Convert(RuleReplacement1);
            List<string> replace = Helpers.Convert(RuleMatch1);
            sb.AppendLine(String.Format("(Loop)\t->\t(Loop)\t:\t{0}\t->\t{1}\t:\t()->()", 
                                                        Helpers.GenerateListItems(match), 
                                                        Helpers.GenerateListItems(replace)));


        }

        public string GenerateMachineRules()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("ContextSensitiveAutomata");
            sb.AppendLine("Theories and Queries");
            sb.AppendLine("Start");
            sb.AppendLine("(Start,Loop,End)");
            sb.AppendLine("($,Red,Blue,Green,Yellow)");
            sb.AppendLine("($)");
            sb.AppendLine("(End)");
            sb.AppendLine("(State_N) -> (StateM) : (InputListMatch) -> (InputListAfterMatch) : (StackListMatch) -> (StackListAfterMatch)");
            sb.AppendLine("(Start)	->	(Loop)	:	()	->	()	:	()	->	($)");
            GenerateSentanceRule(Sentance1, sb);
            GenerateSentanceRule(Sentance2, sb);
            GenerateSentanceRule(Sentance3, sb);
            GenerateRuleStatement(RuleMatch1, RuleReplacement1, sb);
            GenerateRuleStatement(RuleMatch2, RuleReplacement2, sb);
            GenerateRuleStatement(RuleMatch3, RuleReplacement3, sb);
            sb.AppendLine("(Loop)	->	(End)	:	($)	->	()	:	($)	->	()");
            return sb.ToString();
        }



    }
}
