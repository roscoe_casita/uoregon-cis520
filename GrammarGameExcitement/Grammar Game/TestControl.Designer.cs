﻿namespace GrammarGame
{
    partial class TestControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestControl));
            this.colorList1 = new GrammarGame.ColorList();
            this.SuspendLayout();
            // 
            // colorList1
            // 
            this.colorList1.Location = new System.Drawing.Point(3, 3);
            this.colorList1.Name = "colorList1";
            this.colorList1.Size = new System.Drawing.Size(650, 78);
            this.colorList1.TabIndex = 0;
            this.colorList1.TokenCount = 8;
            // 
            // TestControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.colorList1);
            this.Name = "TestControl";
            this.Size = new System.Drawing.Size(656, 87);
            this.ResumeLayout(false);

        }

        #endregion

        private ColorList colorList1;
    }
}
