﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Autonima;

namespace GrammarGame
{
    public partial class GrammarControl : UserControl
    {
        public GrammarControl()
        {
            InitializeComponent();
        }

        public int TokenCount
        {
            get
            {
                int returnValue =
                    S1.RealTokenCount +
                    S2.RealTokenCount +
                    S3.RealTokenCount +
                    R1R.RealTokenCount +
                    R2R.RealTokenCount +
                    R3R.RealTokenCount +
                    R1S.RealTokenCount +
                    R2S.RealTokenCount +
                    R3S.RealTokenCount;
                return returnValue;
            }

        }

        public List<GrammarColor> GetColorsUsed()
        {
                List<GrammarColor> Colors = new List<GrammarColor>();

                Colors.AddRange(S1.GetColorsUsed());
                Colors.AddRange(S2.GetColorsUsed());
                Colors.AddRange(S3.GetColorsUsed());
                Colors.AddRange(R1R.GetColorsUsed());
                Colors.AddRange(R2R.GetColorsUsed());
                Colors.AddRange(R3R.GetColorsUsed());
                Colors.AddRange(R1S.GetColorsUsed());
                Colors.AddRange(R2S.GetColorsUsed());
                Colors.AddRange(R3S.GetColorsUsed());
                return Colors.Distinct().ToList();
            }

        public void SetColorsUsed(List<GrammarColor> value)
        {
            S1.SetColorsUsed(value);
            S2.SetColorsUsed(value);
            S3.SetColorsUsed(value);

            R1R.SetColorsUsed(value);
            R2R.SetColorsUsed(value);
            R3R.SetColorsUsed(value);

            R1S.SetColorsUsed(value);
            R2S.SetColorsUsed(value);
            R3S.SetColorsUsed(value);
            
        }

        public GameRules GetGameRules()
        {
            GameRules gr = new GameRules();

            gr.Sentance1.AddRange(S1.GetListOfColors());
            gr.Sentance2.AddRange(S2.GetListOfColors());
            gr.Sentance3.AddRange(S3.GetListOfColors());

            gr.RuleReplacement1.AddRange(R1R.GetListOfColors());
            gr.RuleReplacement2.AddRange(R2R.GetListOfColors());
            gr.RuleReplacement3.AddRange(R3R.GetListOfColors());

            gr.RuleMatch1.AddRange(R1S.GetListOfColors());
            gr.RuleMatch2.AddRange(R2S.GetListOfColors());
            gr.RuleMatch3.AddRange(R3S.GetListOfColors());

            return gr;
        }

        public void ClearGameRules()
        {
            S1.ClearColors();
            S2.ClearColors();
            S3.ClearColors();

            R1R.ClearColors();
            R2R.ClearColors();
            R3R.ClearColors();

            R1S.ClearColors();
            R2S.ClearColors();
            R3S.ClearColors();

        }

    }
}
