﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Autonima
{
    public class CSATransition
    {
        public readonly string SourceState;
        public readonly string DestinationState;
        public readonly List<string> InputListMatch = new List<string>();
        public readonly List<string> InputAfterMatch = new List<string>();
        public readonly List<string> StackListMatch = new List<string>();
        public readonly List<string> StackAfterMatch = new List<string>();

        public bool DiggingOperation 
        {
            get
            {
                if (InputAfterMatch.Count + InputListMatch.Count == 0)
                    return true;
                if (StackListMatch.Count + StackAfterMatch.Count == 0)
                    return true;
                return false;
            }
        }
        public CSATransition(string read_line)
        {
            // remove all whitespace. 
            read_line = read_line.Replace(" ", "").Replace("\t", "");

            string[] SegmentsAndTransitionsSplitter = { ":", "->" };
            string[] segmentsAndTransitions = read_line.Split(SegmentsAndTransitionsSplitter, StringSplitOptions.RemoveEmptyEntries);


            if (segmentsAndTransitions.Length != 6)
                throw new Exception("Invalid Segment And Transition Count for CSATransition.");


            SourceState = Helpers.ParseListItems(segmentsAndTransitions[0])[0]; // one item.
            DestinationState = Helpers.ParseListItems(segmentsAndTransitions[1])[0]; // should only be one item anyways.
            InputListMatch.AddRange(Helpers.ParseListItems(segmentsAndTransitions[2]));
            InputAfterMatch.AddRange(Helpers.ParseListItems(segmentsAndTransitions[3]));
            StackListMatch.AddRange(Helpers.ParseListItems(segmentsAndTransitions[4]));
            StackAfterMatch.AddRange(Helpers.ParseListItems(segmentsAndTransitions[5]));
            StackAfterMatch.Reverse();

            if (InputAfterMatch.Count + InputListMatch.Count + StackListMatch.Count + StackAfterMatch.Count == 0)
                throw new Exception("Cannot have empty set for all inputs/matches/transitions.");
      /*
       *    if ((InputListMatch.Count + InputAfterMatch.Count == 0) && (SourceState.CompareTo(DestinationState) == 0))
                throw new Exception("Stack Digging Replacements must transition state.");
            if((StackListMatch.Count + StackAfterMatch.Count == 0) && (SourceState.CompareTo(DestinationState)==0))
                throw new Exception("Input Digging Replacements must transition state.");
       */
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            List<string> temp = new List<string>();
            temp.Add(SourceState);
            sb.Append(Helpers.GenerateListItems(temp));
            temp.Clear();
            temp.Add(DestinationState);

            sb.Append("\t->\t");
            sb.Append(Helpers.GenerateListItems(temp));
            sb.Append("\t:\t");
            sb.Append(Helpers.GenerateListItems(InputListMatch));
            sb.Append("\t->\t");
            sb.Append(Helpers.GenerateListItems(InputAfterMatch));
            sb.Append("\t:\t");
            sb.Append(Helpers.GenerateListItems(StackListMatch));
            sb.Append("\t->\t");
            StackAfterMatch.Reverse();
            sb.Append(Helpers.GenerateListItems(StackAfterMatch));
            StackAfterMatch.Reverse();
            return sb.ToString();
        }

        public int IsValidPath(ContextSensitiveComputation csc)
        {
            if (SourceState.CompareTo(csc.CurrentState) != 0)
                return 0;

            if ((InputListMatch.Count == 0) && (InputAfterMatch.Count == 0))
            {
                int matches = 0; 
                List<string> stack = new List<string>(csc.GetStack);

                bool Matched = StackListMatch.Count <= stack.Count;
                for (int i = 0; i + StackListMatch.Count <= stack.Count; i++)
                {
                    Matched = true;
                    for (int j = 0; j < StackListMatch.Count; j++)
                    {
                        if (stack[i + j].CompareTo(StackListMatch[j]) != 0)
                        {
                            Matched = false;
                            break;
                        }
                    }
                    if (Matched)
                    {
                        matches++;
                    }
                }
                return matches;

            }
            else if ((StackListMatch.Count == 0) && (StackAfterMatch.Count == 0))
            {
                List<string> input = new List<string>(csc.GetInputs);

                int matches = 0;
                bool Matched = InputListMatch.Count <= input.Count; 
                for (int i = 0; i + InputListMatch.Count <= input.Count; i++)
                {
                    Matched = true;
                    for (int j = 0; j < InputListMatch.Count; j++)
                    {
                        if (input[i + j].CompareTo(InputListMatch[j]) != 0)
                        {
                            Matched = false;
                            break;
                        }
                    }
                    if (Matched)
                    {
                        matches++;
                    }
                }
                return matches;
                
            }
            else
            {
                if (InputListMatch.Count > csc.GetInputs.Count())
                {
                    return 0;
                }
                if (StackListMatch.Count > csc.GetStack.Count())
                {
                    return 0;
                }
                int i = 0;
                foreach (String s in csc.GetInputs)
                {
                    if (i == InputListMatch.Count)
                        break;
                    if (s.CompareTo(InputListMatch[i++]) != 0)
                        return 0;
                }

                i = 0;
                foreach (String s in csc.GetStack)
                {
                    if (i == StackListMatch.Count)
                        break;
                    if (s.CompareTo(StackListMatch[i++]) != 0)
                        return 0;
                }
            }
            return 1;
        }
    }

    public static class Helpers
    {
        private const string ListBegin = "(";
        private const string ListEnd = ")";
        private const string ListSpliter = ",";
        private static readonly string[] ListSpliters = { "," };

        public static List<string> ParseListItems(string s)
        {
            List<string> returnValue = new List<string>();
            if (!s.StartsWith(ListBegin))
                throw new Exception("List Does not Begin with" + ListBegin);
            if (!s.EndsWith(ListEnd))
                throw new Exception("List Does not End with" + ListEnd);

            s = s.Replace(ListBegin, "").Replace(ListEnd, "");
            if (s.Contains(ListSpliter))
            {
                foreach (string strings in s.Split(ListSpliters, StringSplitOptions.RemoveEmptyEntries))
                {
                    returnValue.Add(strings);
                }
            }
            else if (s.CompareTo("") != 0)
            {
                returnValue.Add(s);
            }
            return returnValue;
        }
        public static string GenerateListItems(IEnumerable<string> items)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ListBegin);

            foreach (string s in items)
            {
                sb.Append(s);
                sb.Append(ListSpliter);
            }
            if (sb.Length > ListSpliter.Length + ListBegin.Length)
            {
                sb.Remove(sb.Length - ListSpliter.Length, ListSpliter.Length);
            }
            sb.Append(ListEnd);
            return sb.ToString();
        }

        public static List<string> Convert(List<GrammarColor> colors)
        {
            List<string> returnValue = new List<string>();

            foreach (GrammarColor gc in colors)
            {
                if (gc.CompareTo(GrammarColor.None) != 0)
                {
                    returnValue.Add(gc.ToString());
                }
            }
            return returnValue;
        }

        public static List<GrammarColor> Convert(List<string> colors)
        {
            List<GrammarColor> returnValue = new List<GrammarColor>();

            foreach (string s in colors)
            {
                Type e = GrammarColor.None.GetType();
                Object o = Enum.Parse(e, s);
                if (o != null)
                {
                    returnValue.Add((GrammarColor)o);
                }

            }
            return returnValue;
        }

        public static bool CompareMultiLists(List<List<string>> s1, List<List<string>> s2)
        {
            if(s1.Count != s2.Count)
                return false;

            foreach(List<string> s_1 in s1)
            {
                bool Matched = false;
                foreach(List<string> s_2 in s2)
                {
                    if(CompareLists(s_1,s_2))
                    {
                        Matched = true;
                        break;
                    }
                }
                if(!Matched)
                    return false;
            }
            return true;
        }

        public static bool CompareLists(List<string> s1, List<string> s2)
        {
            if (s1.Count != s2.Count)
                return false;
            for (int i = 0; i < s1.Count; i++)
            {
                if (s1[i].CompareTo(s2[i]) != 0)
                {
                    return false;
                }
            }
            return true;
        }

    }


    public enum GrammarColor
    {
        None,
        Red,
        Blue,
        Green,
        Yellow
    }

    public class ContextSensitiveAutomata
    {
        public readonly string Name;
        public readonly string StartState;
        public readonly List<string> States = new List<string>();
        public readonly List<string> InputAlphabet = new List<string>();
        public readonly List<string> StackAlphabet = new List<string>();
        public readonly List<string> FinishStates = new List<string>();
        public readonly Dictionary<string, List<CSATransition>> TransitionFunction = new Dictionary<string, List<CSATransition>>();

        public ContextSensitiveAutomata(string s)
        {

            string[] Splitter = { "\r", "\n", "\r\n" };
            List<String> inputs = s.Split(Splitter, StringSplitOptions.RemoveEmptyEntries).ToList();

            if (inputs.Count < 8)
                throw new Exception("Less then Critical Number of line items to create Context Sensitive Automata.");
            if (inputs[0].CompareTo(this.GetType().Name) != 0)
                throw new Exception("Invalid Type Name: " + inputs[0]);

            Name = inputs[1];
            StartState = inputs[2];
            States.AddRange(Helpers.ParseListItems(inputs[3]));
            InputAlphabet.AddRange(Helpers.ParseListItems(inputs[4]));
            StackAlphabet.AddRange(Helpers.ParseListItems(inputs[5]));
            FinishStates.AddRange(Helpers.ParseListItems(inputs[6]));


            // line 7 is == "[SourceState] -> [DestinationState] : [InputListMatch] -> [InputAfterMatch] : [StackListMatch] -> [StackAfterMatch] "
            for (int i = 8; i < inputs.Count; i++)
            {
                CSATransition csat = new CSATransition(inputs[i]);

                if (!TransitionFunction.ContainsKey(csat.SourceState))
                {
                    TransitionFunction.Add(csat.SourceState, new List<CSATransition>());
                }
                TransitionFunction[csat.SourceState].Add(csat);
            }

            /// validation time.
            /// 

            if (!States.Contains(StartState))
                throw new Exception("Start State not contained in States.");

            foreach (string state in FinishStates)
            {
                if (!States.Contains(state))
                {
                    throw new Exception("Finish State Not Contained in States: " + state);
                }
            }

            foreach (List<CSATransition> transitions in TransitionFunction.Values)
            {
                bool ContainsDiggerOperation = false;
                foreach (CSATransition transition in transitions)
                {
                    if (!States.Contains(transition.SourceState))
                        throw new Exception("States does not contain Source Transition: " + transition.SourceState);
                    if (!States.Contains(transition.DestinationState))
                        throw new Exception("States does not contain Source Transition: " + transition.DestinationState);

                    foreach (string input in transition.InputAfterMatch)
                        if (!InputAlphabet.Contains(input))
                            throw new Exception("Input Alphabet Does not Contain Input After Match: " + input);
                    foreach (string input in transition.InputListMatch)
                        if (!InputAlphabet.Contains(input))
                            throw new Exception("Input Alphabet Does not Contain Input List Match: " + input);
                    foreach (string stack in transition.StackListMatch)
                        if (!StackAlphabet.Contains(stack))
                            throw new Exception("Stack Alphabet Does not Contain Stack List Match: " + stack);
                    foreach (string stack in transition.StackAfterMatch)
                        if (!StackAlphabet.Contains(stack))
                            throw new Exception("Stack Alphabet Does not Contain Stack After Match: " + stack);
                    if (transition.DiggingOperation)
                    {
                        if (ContainsDiggerOperation)
                        {
                            //throw new Exception("State Can only have 1 digging operation.");
                        }
                        else
                        {
                            ContainsDiggerOperation = true;
                        }
                    }
                }
            }

            // pretty valid at this point.
        }

        private string HelpString = "(State_N) -> (StateM) : (InputListMatch) -> (InputListAfterMatch) : (StackListMatch) -> (StackListAfterMatch)";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(this.GetType().Name);
            sb.AppendLine(Name);
            sb.AppendLine(StartState);
            sb.AppendLine(Helpers.GenerateListItems(States));
            sb.AppendLine(Helpers.GenerateListItems(InputAlphabet));
            sb.AppendLine(Helpers.GenerateListItems(StackAlphabet));
            sb.AppendLine(Helpers.GenerateListItems(FinishStates));
            sb.AppendLine(HelpString);
            foreach (List<CSATransition> transitions in TransitionFunction.Values)
            {
                foreach (CSATransition transition in transitions)
                {
                    sb.AppendLine(transition.ToString());
                }
            }
            sb.AppendLine();
            return sb.ToString();
        }

        public bool TestString(string s, out string OutputProcessor)
        {
            List<string> strings = new List<string>();

            string[] splitter = { ":", ".", " " };

            if (s.Contains(" ") ||
                s.Contains(":") ||
                s.Contains("."))
            {
                strings.AddRange(s.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
            }
            else
            {
                foreach (char c in s)
                {
                    string temp = "" + c;
                    strings.Add(temp);
                }
            }


            return Parser.ParseString(this, strings, out OutputProcessor);
        }

        public bool BetterTest(List<string> input,out string sb)
        {
            return Parser.ParseString(this, input,out sb);
        }

        public int GetStackRuleCount
        {
            get
            {
                int i = 0;
                foreach (List<CSATransition> transitions in TransitionFunction.Values)
                {
                    foreach (CSATransition transition in transitions)
                    {
                        i += transition.StackAfterMatch.Count;
                    }
                }
                return i;
            }
        }
        public int GetInputRuleCount
        {
            get
            {
                int i = 0;
                foreach (List<CSATransition> transitions in TransitionFunction.Values)
                {
                    foreach (CSATransition transition in transitions)
                    {
                        i += transition.InputAfterMatch.Count;
                    }
                }
                return i;
            }
        }
        NonDeterministicContextSensitiveParser Parser = new NonDeterministicContextSensitiveParser();


        // generate a pretty picture with this.
        static GraphVizWrapper.IGraphGeneration Igraph;

        // generate a the byte stream.
        public byte[] GetImage()
        {

            if (Igraph == null)
            {
                GraphVizWrapper.Queries.GetProcessStartInfoQuery gpsiq = new GraphVizWrapper.Queries.GetProcessStartInfoQuery();
                GraphVizWrapper.Queries.GetStartProcessQuery gspq = new GraphVizWrapper.Queries.GetStartProcessQuery();
                GraphVizWrapper.Commands.RegisterLayoutPluginCommand rlpc = new GraphVizWrapper.Commands.RegisterLayoutPluginCommand(gpsiq, gspq);
                Igraph = new GraphVizWrapper.GraphGeneration(gspq, gpsiq, rlpc);

            }
            string graph = BuildGraphVizDisplay();
            return Igraph.GenerateGraph(graph, GraphVizWrapper.Enums.GraphReturnType.Jpg);
        }

        // here we generate the graph of the PDA
        public String BuildGraphVizDisplay()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("digraph \"" + Name + "\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"" + Name + "," + DateTime.Now.ToShortDateString() + "\",");
            sb.AppendLine("size = \"12,12\"];");
            sb.AppendLine("secret_node [style=invis];");
            foreach (string state in States)
            {
                string shape = "circle";

                if (FinishStates.Contains(state))
                {
                    shape = "doublecircle";
                }
                sb.AppendFormat("{0} [label=\"{1}\",shape={2}];\r\n", state.Replace(" ", ""), state, shape);
            }

            // graph viz needs this double encoded new line.
            ///string newline = "\\n";

            foreach (KeyValuePair<string, List<CSATransition >> state in TransitionFunction)
            {
                Dictionary<string, StringBuilder> StateTransitions = new Dictionary<string, StringBuilder>();

                foreach (CSATransition csat in state.Value)
                {
                    if (!StateTransitions.ContainsKey(csat.DestinationState))
                    {
                        StateTransitions.Add(csat.DestinationState, new StringBuilder());
                    }
                    StateTransitions[csat.DestinationState].AppendFormat("{0}->{1} : {2}->{3}\\n",
                                                                                    Helpers.GenerateListItems(csat.InputListMatch),
                                                                                    Helpers.GenerateListItems(csat.InputAfterMatch),
                                                                                    Helpers.GenerateListItems(csat.StackListMatch),
                                                                                    Helpers.GenerateListItems(csat.StackAfterMatch.AsEnumerable().Reverse())
                                                                                    );
                }
                foreach (KeyValuePair<string, StringBuilder> t in StateTransitions)
                {
                    sb.AppendFormat("{0} -> {1} [label=\"{2}\"];\r\n", state.Key.Replace(" ", ""), t.Key.Replace(" ", ""), t.Value.ToString());
                }
            }
            sb.AppendLine("secret_node -> " + StartState.Replace(" ", "") + "[label=\"start\"];");
            sb.AppendLine("}");
            return sb.ToString();
        }
    }

    public class ContextSensitiveComputation
    {
        public ContextSensitiveComputation(string state, IEnumerable<string> inputs)
        {
            State = state;
            Inputs.AddRange(inputs);
        }


        public ContextSensitiveComputation Branch()
        {
            ContextSensitiveComputation c = new ContextSensitiveComputation(this.State, this.Inputs);

            foreach (string s in Stack.Reverse())
            {
                c.Stack.Push(s);
            }
            c.Transitions.AddRange(Transitions);
            return c;
        }

        public string CurrentState { get { return State; } }
        public IEnumerable<string> GetInputs { get { return Inputs; } }
        public IEnumerable<string> GetStack { get { return Stack; } }
        public IEnumerable<CSATransition> GetTransitions { get { return Transitions; } }

        public int GetStackDepth { get { return Stack.Count; } }

        public void DoTransition(CSATransition transition,int match_number)
        {
            if (State.CompareTo(transition.SourceState) != 0)
                throw new Exception("Invalid Transition.");

            if ((transition.InputListMatch.Count == 0) && (transition.InputAfterMatch.Count == 0))
            {

                List<string> stack = new List<string>();

                while (Stack.Count > 0)
                {
                    stack.Add(Stack.Pop());
                }
                for (int i = 0; i + transition.StackListMatch.Count <= stack.Count; i++)
                {
                    bool Matched = true;
                    for (int j = 0; j < transition.StackListMatch.Count; j++)
                    {
                        if (stack[i + j].CompareTo(transition.StackListMatch[j]) != 0)
                        {
                            Matched = false;
                            break;
                        }
                    }
                    if (Matched == true)
                    {
                        if (match_number-- == 0)
                        {
                            stack.RemoveRange(i, transition.StackListMatch.Count);
                            stack.InsertRange(i, transition.StackAfterMatch.AsEnumerable().Reverse());
                            i = 0;
                            break;
                        }
                    }
                }
                stack.Reverse();
                foreach (string s in stack)
                {
                    Stack.Push(s);
                }
            }
            else if ((transition.StackListMatch.Count == 0) && (transition.StackAfterMatch.Count == 0))
            {
                for (int i = 0; i + transition.InputListMatch.Count <= Inputs.Count; i++)
                {
                    bool Matched = true;
                    for (int j = 0; j < transition.InputListMatch.Count; j++)
                    {
                        if (Inputs[i + j].CompareTo(transition.InputListMatch[j]) != 0)
                        {
                            Matched = false;
                            break;
                        }
                    }
                    if (Matched == true)
                    {
                        if (match_number-- == 0)
                        {
                            Inputs.RemoveRange(i, transition.InputListMatch.Count);
                            Inputs.InsertRange(i, transition.InputAfterMatch);
                            i = 0;
                            break;
                        }
                    }
                }
            }
            else
            {
                if (transition.InputListMatch.Count > 0)
                {
                    foreach (string s in transition.InputListMatch)
                    {
                        if (Inputs[0].CompareTo(s) != 0)
                            throw new Exception("Invalid Transition.");
                        Inputs.RemoveAt(0);
                    }
                }

                if (transition.InputAfterMatch.Count > 0)
                {
                    Inputs.InsertRange(0, transition.InputAfterMatch);
                }

                if (transition.StackListMatch.Count > 0)
                {
                    foreach (string s in transition.StackListMatch)
                    {
                        if (Stack.Pop().CompareTo(s) != 0)
                            throw new Exception("Invalid Stack List Match.");
                    }
                }

                if (transition.StackAfterMatch.Count > 0)
                {
                    foreach (string s in transition.StackAfterMatch)
                    {
                        Stack.Push(s);
                    }
                }
            }
            State = transition.DestinationState;
            Transitions.Add(transition);
        }

        string State = null;
        List<string> Inputs = new List<string>();
        Stack<string> Stack = new Stack<string>();
        List<CSATransition> Transitions = new List<CSATransition>();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Helpers.GenerateListItems(Inputs));
            sb.Append(",");
            sb.Append(Helpers.GenerateListItems(Stack));
            return sb.ToString();
        }


    }

    public class NonDeterministicContextSensitiveParser
    {

        public bool ParseString(ContextSensitiveAutomata contextSensitiveAutomata, List<string> strings, out string OutputProcessor)
        {
            StringBuilder sb = new StringBuilder();

            ContextSensitiveComputation comp = new ContextSensitiveComputation(contextSensitiveAutomata.StartState, strings);

            List<ContextSensitiveComputation> Computations = new List<ContextSensitiveComputation>();
            Computations.Add(comp);
            List<ContextSensitiveComputation> NextComputations = new List<ContextSensitiveComputation>();
            List<ContextSensitiveComputation> Swap = null;
            List<ContextSensitiveComputation> FinalComputations = new List<ContextSensitiveComputation>();

            bool Done = false;

            int MaxStackDepth = strings.Count + contextSensitiveAutomata.GetStackRuleCount + 1;
            int MaxInputDepth = strings.Count + contextSensitiveAutomata.GetInputRuleCount + 1;

            while (!Done)
            {
                sb.AppendLine("----------------------------------------------------");
                foreach (ContextSensitiveComputation csc in Computations)
                {
                    if (contextSensitiveAutomata.FinishStates.Contains(csc.CurrentState))
                    {
                        if ((csc.GetInputs.Count() == 0) && (csc.GetStack.Count() == 0))
                        {
                            FinalComputations.Add(csc);
                            break;
                        }
                    }
                    else if (contextSensitiveAutomata.TransitionFunction.ContainsKey(csc.CurrentState))
                    {
                        bool Removed = true;
                        if ( (csc.GetStackDepth < MaxStackDepth) && (csc.GetInputs.Count() < MaxInputDepth))
                        {
                            foreach (CSATransition csat in contextSensitiveAutomata.TransitionFunction[csc.CurrentState])
                            {
                                int branchCount = csat.IsValidPath(csc);
                                if (branchCount > 0)
                                {
                                    Removed = false;

                                    for (int i = 0; i < branchCount; i++)
                                    {
                                        ContextSensitiveComputation next = csc.Branch();

                                        next.DoTransition(csat,i);
                                        NextComputations.Add(next);

                                        sb.AppendLine("___________");

                                        sb.AppendLine("Adv: " + csc.ToString());
                                        sb.AppendLine("Via: " + csat.ToString());
                                        sb.AppendLine("To:  " + next.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            sb.AppendLine("____________");
                            sb.AppendLine("Removed computation due to excessive stack depth without matches.");
                            Removed = true;
                        }
                        if (Removed)
                        {
                            sb.AppendLine("_____________");
                            sb.AppendLine("Removing Dead Computation: " + csc.ToString());
                        }
                    }
                    else
                    {
                        sb.AppendLine("_____________");
                        sb.AppendLine("Removing Dead Computation: " + csc.ToString());
                    }

                }


                Swap = Computations;
                Computations = NextComputations;
                NextComputations = Swap;
                NextComputations.Clear();

                if( (Computations.Count == 0) || (FinalComputations.Count > 0))
                {
                    Done = true;
                }
            }
            sb.AppendLine("-----------------------------------------------");

            bool returnValue = false;

            if (FinalComputations.Count > 0)
            {
                sb.AppendLine("Total Computation Paths Found: " + FinalComputations.Count);
                foreach (ContextSensitiveComputation csc in FinalComputations)
                {
                    sb.AppendLine("---------");
                    sb.AppendLine("Path:");
                    foreach (CSATransition csat in csc.GetTransitions)
                    {
                        sb.AppendLine(csat.ToString());
                    }
                }
                returnValue = true;
            }
            else
            {
                returnValue = false;
                sb.AppendLine("No Computations completed to final state.");
            }

            sb.AppendLine("-----------------------------------------------");
            OutputProcessor = sb.ToString();
            return returnValue;
        }
    }
}
