﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Autonima
{

    /// <summary>
    /// This class handles the transition function states for each transition:
    /// 
    ///     qFrom,InputMatch,qTo.
    ///     
    /// While they could be in a strict tuple, we designate a few additional group operators for easier code handling:
    ///     Read, Write for saving/restoring the transition to/from a file.
    /// </summary>
    public class FiniteTransition
    {
        public readonly string SourceState;
        public readonly string InputMatch;
        public readonly string DestinationState;


        // Read the Transition from a file line.
        public static FiniteTransition Read(StreamReader sr)
        {
            string temp = sr.ReadLine();

            // Easy test, just so that things are mostly formated right.
            if ((temp.Contains(StartStateDelimiter) == false) ||
                (temp.Contains(InputDestinationDelimiter) == false))
            {
                throw new Exception("Incorrectly formated Finite Transition function table Entry: " + temp);
            }

            // ensure we can handle the lambda transition. if StringSplitOptions.RemoveEmpty, then we would loose "" transitions.
            string[] temps = temp.Split(Delimiters, StringSplitOptions.None);

            if (temps.Length != 3)
            {
                throw new Exception("Incorrect count of items Finite Transition function table Entry: " + temp);
            }

            return new FiniteTransition(temps[0], temps[1], temps[2]);
        }

        // Write the transition to the file.
        public static void Write(FiniteTransition ft, StreamWriter sw)
        {
            string temp = string.Format("{0}:{1}->{2}", ft.SourceState, ft.InputMatch, ft.DestinationState);
            sw.WriteLine(temp);
        }

        // we can only create this privately, this way we are always ensured of the correct read/write functions.
        private FiniteTransition(string source_state, string input_match, string destination_state)
        {
            SourceState = source_state;
            InputMatch = input_match;
            DestinationState = destination_state;
        }
        private static string[] Delimiters = { ":", "->"};
        private const string StartStateDelimiter = ":";
        private const string InputDestinationDelimiter = "->";
    }

    /// <summary>
    /// this is the 'master' finite automata class, there is actually  a 'type' handler that does non-deterministic / deterministic handling
    /// This only handles the reading/writing/validation.
    /// 
    /// </summary>
    public class FiniteAutomata
    {
        // there is no name in the formal 5 tuple, but its really nice for diagrams and distinction.
        public string Name = "";
        public string StartState = "";
        public List<string> States = new List<string>();
        public List<string> Alphabet = new List<string>();
        public List<string> AcceptStates = new List<string>();
        public Dictionary<string, List<FiniteTransition>> TransitionFunction = new Dictionary<string, List<FiniteTransition>>();



        // the group operator that will tell you if a string is in the FA, it will also create some nice pretty print dumps for you.
        public static bool ParseString(FiniteAutomata fa, string s, out String parser_text)
        {

            List<string> inputs = new List<string>();

            // if the string contains our delimiter, then we chop it up
            if (s.Contains(state_delimiter))
            {
                inputs.AddRange(s.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
            }
            else
            {
                // otherwise lets just chop it up into characters.
                foreach (char c in s)
                {
                    string s1 = "" + c;
                    inputs.Add(s1);
                }
            }


            // use our actual type parser, and parse it.
            return fa.TypeParser.Parse(fa,inputs,out parser_text);
        }

        // this is used to differentiate the NFA/DFA/ etc.
        private FiniteAutomataParser TypeParser = null;


        private static string[] splitter = { ":" };
        private static string state_delimiter = ":";

        // this way you can only restore/save the FA.
        private FiniteAutomata()
        {
        }

        /// <summary>
        /// Read the Finite Automata from a file.
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        public static FiniteAutomata Read(StreamReader sr)
        {
            // DFA, NFA, all-nfa etc.
            string TypeName = sr.ReadLine();

            /// initilize the return value.
            FiniteAutomata returnValue = new FiniteAutomata();
            switch (TypeName)
            {
                    // DFA, get a dfa parser.
                case "DeterministicFiniteAutomata":
                    returnValue.TypeParser = new DeterministicFiniteAutomata();
                    break;
                    // NFA, get a NFA parser.
                case "NonDeterministicFiniteAutomata":
                    returnValue.TypeParser = new NonDeterministicFiniteAutomata();
                    break;
                //case "AllNonDeterministicFiniteAutomata":
                //    returnValue.TypeParser = new AllNonDeterministicFiniteAutomata();
                //    break;
                default:
                    throw new Exception("Wrong Type of Finite Automata Encountered: " + TypeName);
            }
            //name of the FA
            returnValue.Name = sr.ReadLine();
            //the starting state to being from.
            returnValue.StartState = sr.ReadLine();
            // the list of all states.
            returnValue.States = sr.ReadLine().Split(splitter, StringSplitOptions.None).ToList();
            // the alphabet of strings.
            returnValue.Alphabet = sr.ReadLine().Split(splitter, StringSplitOptions.None).ToList();
            // the list of states that we can finish in.
            returnValue.AcceptStates = sr.ReadLine().Split(splitter, StringSplitOptions.None).ToList();

            // the transition table.
            while (!sr.EndOfStream)
            {
                // each transition function,   "State:Alphabet:State"

                FiniteTransition fa = FiniteTransition.Read(sr);

                // if the transition function dictionary does not contains the state, add the state.
                if (!returnValue.TransitionFunction.ContainsKey(fa.SourceState))
                {
                    returnValue.TransitionFunction.Add(fa.SourceState, new List<FiniteTransition>());
                }
                // add the transition to the state.
                returnValue.TransitionFunction[fa.SourceState].Add(fa);
            }

            // now we do some simple validation:

            // test if the start state is in the states.
            if (!returnValue.States.Contains(returnValue.StartState))
            {
                throw new Exception("States Does not contain the Start State: " + returnValue.StartState);
            }

           // test that each final state is in the states.
            foreach (string state in returnValue.AcceptStates)
            {
                if (!returnValue.States.Contains(state))
                {
                    throw new Exception("States Does Not Contain the Accpet State: " + state);
                }
            }

            // for all transitions, in all state,
            foreach(List<FiniteTransition> fts in returnValue.TransitionFunction.Values)
            {
                foreach (FiniteTransition ft in fts)
                {
                    // source state needs to be in the transition table,
                    if (!returnValue.States.Contains(ft.SourceState))
                        throw new Exception("States Does Not Contain the state: " + ft.SourceState);
                    // destination state needs to be in the transition table,
                    if (!returnValue.States.Contains(ft.DestinationState))
                        throw new Exception("States Does Not Contain the state: " + ft.DestinationState);
                    // alphabet needs to be contained in the alphabet.
                    if (!returnValue.Alphabet.Contains(ft.InputMatch))
                        throw new Exception("Alphabet Does Not Contain the string: " + ft.InputMatch);
                }
            }
            // parser has its own validation it needs to do.
            returnValue.TypeParser.Validate(returnValue);
            return returnValue;
        }

        // image generator.
        static GraphVizWrapper.IGraphGeneration Igraph;

        // get the image as a byte array.
        public static byte[] GetImage(FiniteAutomata fa)
        {
            
            if (Igraph == null)
            {
                GraphVizWrapper.Queries.GetProcessStartInfoQuery gpsiq = new GraphVizWrapper.Queries.GetProcessStartInfoQuery();
                GraphVizWrapper.Queries.GetStartProcessQuery gspq = new GraphVizWrapper.Queries.GetStartProcessQuery();
                GraphVizWrapper.Commands.RegisterLayoutPluginCommand rlpc = new GraphVizWrapper.Commands.RegisterLayoutPluginCommand(gpsiq,gspq);
                Igraph = new GraphVizWrapper.GraphGeneration(gspq, gpsiq, rlpc);
                
            }
            string graph = BuildGraphVizDisplay(fa);            
            return Igraph.GenerateGraph(graph, GraphVizWrapper.Enums.GraphReturnType.Jpg);
        }

        // this will escape a string "" to "\epsilon"
        private static string Epsiloned(string s)
        {
            if(s.CompareTo("")==0)
                return "\u03B5";
            else
                return s;

        }

        // generate the formate expected by Graphviz.
        public static String BuildGraphVizDisplay(FiniteAutomata fa)
        {
            StringBuilder sb = new StringBuilder();

            // now the name is nice.
            sb.AppendLine("digraph \"" + fa.Name + "\" {");

            // generate a header.
            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"" + fa.Name + "," + DateTime.Now.ToShortDateString() + "\",");
            sb.AppendLine("size = \"12,12\"];");

            // this gives us a 'line in' to the start state.
            sb.AppendLine("secret_node [style=invis];");
            foreach (string state in fa.States)
            {
                string shape = "circle";

                if (fa.AcceptStates.Contains(state))
                {
                    shape = "doublecircle";
                }
                // generate each state, either as a normal, or accept state.
                sb.AppendFormat("{0} [label=\"{1}\",shape={2}];\r\n",state.Replace(" ",""),state,shape);
            }


            // generate each transition now.
            // we do this state by state.
            foreach (KeyValuePair<string,List<FiniteTransition>> fsts in fa.TransitionFunction)
            {
                // we really only want one line for each transition, so we need to do some extra work here.
                // this will be the text on the edge.
                Dictionary<string, StringBuilder> StateTransitions = new Dictionary<string, StringBuilder>();

                // loop through each transition
                foreach (FiniteTransition ft in fsts.Value)
                {
                    // if the out table does not contain the destination, add it.
                    if (!StateTransitions.ContainsKey(ft.DestinationState))
                    {
                        StateTransitions.Add(ft.DestinationState, new StringBuilder());
                    }
                    // append this transition text.
                    StateTransitions[ft.DestinationState].AppendFormat("{0},", Epsiloned(ft.InputMatch));
                }

                // now we can actually generate each line item.
                foreach(KeyValuePair<string,StringBuilder> t in StateTransitions)
                {
                    // remove the last ','
                    t.Value.Remove(t.Value.Length - 1,1);

                    // generate a transition edge from source to destination, with all valid values on the edge transition.
                    sb.AppendFormat("{0} -> {1} [label=\"{2}\"];\r\n", fsts.Key.Replace(" ", ""), t.Key.Replace(" ", ""), t.Value.ToString());
                }
            }
            // add the secret node transition. this gives us an in-line to the start node. 
            sb.AppendLine("secret_node -> " + fa.StartState.Replace(" ", "") + "[label=\"start\"];");
            sb.AppendLine("}");
            return sb.ToString();
        }

        // write the entrie FA out to a file.
        public static void Write(FiniteAutomata fa, StreamWriter sw)
        {
            // type name for the type parser.
            sw.WriteLine(fa.TypeParser.TypeName);
            // our name.
            sw.WriteLine(fa.Name);
            // start state.
            sw.WriteLine(fa.StartState);

            // each state.
            string s = BuildStringList(fa.States);
            sw.WriteLine(s);
            // each alphabet item.
            s = BuildStringList(fa.Alphabet);
            sw.WriteLine(s);
            // each accept state.
            s = BuildStringList(fa.AcceptStates);
            sw.WriteLine(s);

            // each line item in the transition function. 
            foreach (List<FiniteTransition> fts in fa.TransitionFunction.Values)
            {
                foreach (FiniteTransition ft in fts)
                {
                    // the transition line item knows how to write itself.
                    FiniteTransition.Write(ft, sw);
                }
            }
        }

        // this just helps us build our lists to write out using the same delimiter for everything.
        static string BuildStringList(IEnumerable<string> strings)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in strings)
            {
                sb.Append(s);
                sb.Append(state_delimiter);
            }
            sb.Remove(sb.Length - state_delimiter.Length, state_delimiter.Length);
            return sb.ToString();
        }
    }


    /// <summary>
    /// This is where the distinction between NFA/DFA/All-NFA happens.
    /// This is just the interface we define so that it can be called consistantly, without problems.
    /// </summary>
    public interface FiniteAutomataParser
    {
        /// <summary>
        /// This is used to identify the name of the parser, it can be derived with this.Type.TypeName.
        /// </summary>
        string TypeName { get; }
        /// <summary>
        /// This will parse an input, with a given finite Automata, and print to a output buffer.
        /// </summary>
        /// <param name="fa"></param>
        /// <param name="inputs"></param>
        /// <param name="ParserOutput"></param>
        /// <returns></returns>
        bool Parse( FiniteAutomata fa,List<string> inputs,out string ParserOutput);

        /// <summary>
        /// some parsers have additional specifications that must be met.
        /// </summary>
        /// <param name="fa"></param>
        /// <returns></returns>
        bool Validate(FiniteAutomata fa);
    }

    /// <summary>
    /// DFA
    /// </summary>
    public class DeterministicFiniteAutomata : FiniteAutomataParser
    {

        /// <summary>
        ///  Simple validation for DFA
        /// </summary>
        /// <param name="fa"></param>
        /// <returns></returns>
        public bool Validate(FiniteAutomata fa)
        {
            // no epsilons allowed.
            if(fa.Alphabet.Contains(""))
                throw new Exception("Alphabet cannot contain Epsilon.");

            Dictionary<string,List<string>> tester = new Dictionary<string,List<string>>();

            foreach (KeyValuePair<string,List<FiniteTransition>> state in fa.TransitionFunction)
            {
                tester.Add(state.Key, new List<string>());

                foreach (FiniteTransition transition in state.Value)
                {
                    tester[state.Key].Add(transition.InputMatch);
                }
            }

            // every state, must have a transition, for every alphabet value.
            foreach (KeyValuePair<string, List<string>> kvp in tester)
            {
                foreach (string s in fa.Alphabet)
                {
                    if (!kvp.Value.Contains(s))
                        throw new Exception("State " + kvp.Key + "Does not contain transition for " + s);
                }
            }

            return true;
        }


        /// <summary>
        /// This will take a string, a FA statemachine, a output processor, and return a truth value.
        /// </summary>
        /// <param name="fa"></param>
        /// <param name="input"></param>
        /// <param name="ParserOutput"></param>
        /// <returns></returns>
        public bool Parse(FiniteAutomata fa, List<string> input, out string ParserOutput)
        {
            // validate that the input is contained in the alphabet before doing anything.
            foreach (string s in input)
            {
                if (!fa.Alphabet.Contains(s))
                {
                    ParserOutput = "Invalid Alphabetic Input :" + s;
                    return false;
                }
            }

            bool returnSuccess = false;
            StringBuilder returnValue = new StringBuilder();
            string currentState = fa.StartState;

            // this way, if the start state is an accept state, and the input is null, we return true.
            bool Done = input.Count <1;


            while (!Done)
            {
                // get the current input.
                string input_match = input[0];

                // from the current state
                foreach (FiniteTransition transition in fa.TransitionFunction[currentState])
                {
                    // find the state transition that matches the given input.
                    if (input_match.CompareTo(transition.InputMatch) == 0)
                    {
                        // print to the output processor.
                        returnValue.AppendFormat("{0}:[{1}]->{2}\r\n", currentState, input_match, transition.DestinationState);

                        // remove the current input.
                        input.RemoveAt(0);
                        // transition to the new state.
                        currentState = transition.DestinationState;
                        // stop processing.
                        break;
                    }
                }
                // if there is no input left, we are done.
                if (input.Count == 0)
                {
                    Done = true;
                }
            }

            // if the final state is an accept state,
            if (fa.AcceptStates.Contains(currentState))
            {
                // output some good values.
                returnValue.AppendLine("DeterministicFiniteAutomata Accepts " + currentState);
                // set the truth value to true.
                returnSuccess = true;
            }
            else
            {
                // output some bad values.
                returnValue.AppendLine("DeterministicFiniteAutomata Rejects " + currentState);
                // set the truth value to false.
                returnSuccess = false;
            }

            ParserOutput = returnValue.ToString();
            return returnSuccess;

        }


        // we could also return This.Type.Name.
        public string TypeName
        {
            get { return "DeterministicFiniteAutomata"; }
        }
    }

    /// <summary>
    /// NFA, nondeterministic for reals.
    /// </summary>
    public class NonDeterministicFiniteAutomata : FiniteAutomataParser
    {

        // This.Type.Name.
        public string TypeName
        {
            get { return "NonDeterministicFiniteAutomata"; }
        }

        // this is used internally to get all the transition out of a state.
        List<string> EpsilonTransitions(string state, FiniteAutomata fa)
        {
            // the list of values to transition to.
            List<string> returnValue = new List<string>();


            // this represents the state we are transitioning to.
            returnValue.Add(state);
           
            // from the destination state,
            foreach (FiniteTransition transition in fa.TransitionFunction[state])
            {
                // all transitions from that state that are epsilon transitions, can be taken,
                if (transition.InputMatch.CompareTo("")==0)
                {
                    // so take them,
                    returnValue.Add(transition.DestinationState);
                }
            }
            

            // all possible transitions from the state we are transitioning to.
            return returnValue;
        }

        /// <summary>
        ///  Take a given input, a finite automata, an output processor, and return the truthiness of the computation.
        /// </summary>
        /// <param name="fa"></param>
        /// <param name="input"></param>
        /// <param name="ParserOutput"></param>
        /// <returns></returns>
        public bool Parse(FiniteAutomata fa, List<string> input, out string ParserOutput)
        {
            // ensure all strings are in the alphabet. this is a lot cleaner then trying to implement it inside the loop.
            // yes its turns it into a 2*N process vs a N process. 
            foreach (String s in input)
            {
                if (!fa.Alphabet.Contains(s))
                {
                    ParserOutput = "Invalid Alphabet item: " + s;
                    return false;
                }
            }

            StringBuilder returnValue = new StringBuilder();

            List<string> currentStates = new List<string>();

            // from the start state, add all epsilon transitions.
            currentStates.AddRange(EpsilonTransitions(fa.StartState, fa));

            List<string> new_states = new List<string>();
            List<string> swap = null;

            // this way, if the start state is an accept state, and the input is null, we return true.

            bool Done = input.Count < 1;
            
            while (!Done)
            {
                // get the current input.
                string word = input[0];

                // loop though all the 'possible' non-deterministic states.
                foreach (string current_state in currentStates)
                {
                    // do we reject this state as only having invalid transitions out?
                    bool Reject = true;
                    foreach (FiniteTransition transition in fa.TransitionFunction[current_state])
                    {
                        // if the transition out, matches, (could be multiple matches...)
                        if (transition.InputMatch.CompareTo(word) == 0)
                        {
                            // find all the destination states, (ones that can be transitioned to via epsilon)
                            List<string> transitions = EpsilonTransitions(transition.DestinationState, fa);


                            foreach (string t in transitions)
                            {
                                // print out the transition
                                returnValue.AppendFormat("{0}:[{1}]->{2}\r\n", current_state, word, t);

                                // add it to the new states list. we will trim based on multiple states in the same place.
                                if (!new_states.Contains(t))
                                {
                                    // all DFA/NFA don't need 2 states for any given input point of time.
                                    new_states.Add(t);
                                }
                            }
                            Reject = false;
                        }
                    }
                    if (Reject)
                    {
                        // hey, we couldn't get anywhere from this transition, we are going to remove it. 
                        returnValue.AppendLine("Removing Invalid state " + current_state + " With input: " + word);
                    }

                }
                // this delimits each run through the machine.
                returnValue.AppendLine("-------------------------------------");
                swap = currentStates;
                currentStates = new_states;
                new_states = swap;
                new_states.Clear();

                input.RemoveAt(0);

                if (currentStates.Count == 0 || input.Count == 0)
                {
                    Done = true;
                }
            }

            bool returnSuccess = false;


            // if any states is in the accept state, then we are gtg.
            for (int i = 0; i < currentStates.Count; i++)
            {
                if (fa.AcceptStates.Contains(currentStates[i]))
                {
                    returnValue.AppendFormat("NonDeterministicFiniteAutomata Accepts: [{0}]\"{1}\"\r\n", i, currentStates[i]);
                    returnSuccess = true;
                }
            }
            // otherwise, all stats are in a bad state.
            if (!returnSuccess)
            {
                returnValue.AppendLine("NonDeterministicFiniteAutomata Rejects, no valid states.");
            }

            // write the output. 
            
            ParserOutput = returnValue.ToString();

            // return truthiness value.
            return returnSuccess;

        }


        // no special extra considerations. 
        public bool Validate(FiniteAutomata fa)
        {
           return true;
        }
    }

    // the all-NFA does everything the NFA does... but the accept routine makes sure that ALL the states are valid instead. 
    //public class AllNonDeterministicFiniteAutomata : FiniteAutomataParser
    //{

    //    public string TypeName
    //    {
    //        get { return "AllNonDeterministicFiniteAutomata"; }
    //    }


    //    public bool Validate(FiniteAutomata fa)
    //    {
    //        return true;
    //    }

    //    List<string> AddEpsilonTransition(string state, FiniteAutomata fa)
    //    {
    //        List<string> returnValue = new List<string>();
    //        foreach (KeyValuePair<string, List<string>> transition in fa.TransitionFunction[state])
    //        {
    //            if (transition.Value.Contains(""))
    //            {
    //                returnValue.Add(transition.Key);
    //            }
    //        }
    //        return returnValue;
    //    }

    //    public string Parse(List<string> inputs, FiniteAutomata fa)
    //    {
    //        StringBuilder returnValue = new StringBuilder();

    //        List<string> input = inputs;
    //        List<string> currentStates = new List<string>();
    //        currentStates.Add(fa.StartState);
    //        currentStates.AddRange(AddEpsilonTransition(fa.StartState, fa));
    //        foreach (string s in currentStates)
    //        {
    //            returnValue.AppendLine(s);
    //        }
    //        List<string> add_states = new List<string>();
    //        while (input.Count > 0 && currentStates.Count > 0)
    //        {
    //            bool advance = false;
    //            List<int> remove = new List<int>();
    //            for (int i = 0; i < currentStates.Count; i++)
    //            {
    //                if (fa.TransitionFunction.ContainsKey(currentStates[i]))
    //                {
    //                    foreach (KeyValuePair<string, List<string>> transition in fa.TransitionFunction[currentStates[i]])
    //                    {
    //                        bool Reject = true;
    //                        foreach (string word in transition.Value)
    //                        {
    //                            if (input[0].CompareTo(word) == 0)
    //                            {
    //                                Reject = false;
    //                                advance = true;
    //                                returnValue.AppendFormat("{0}[{1}] -> {2}\r\n", currentStates[i], word, transition.Key);

    //                                List<string> temp_states = AddEpsilonTransition(transition.Key, fa);

    //                                if (temp_states.Count > 0)
    //                                {
    //                                    foreach (string new_state in temp_states)
    //                                    {
    //                                        returnValue.AppendFormat("{0}[{1}] -> {2}\r\n", currentStates[i], word, new_state);
    //                                        add_states.Add(new_state);
    //                                    }

    //                                }
    //                                currentStates[i] = transition.Key;
    //                                break;
    //                            }
    //                        }
    //                        if (Reject)
    //                        {
    //                            returnValue.AppendLine("Invalid Transition " + input[0] + " from state: " + currentStates[i]);
    //                            currentStates[i] = "";
    //                        }
    //                    }
    //                }
    //            }
    //            if (!advance)
    //            {
    //                returnValue.AppendLine("Invalid Input String Not Contained in Alphabet: " + input[0]);
    //                break;
    //            }
    //            input.RemoveAt(0);
    //            currentStates.AddRange(add_states);
    //            add_states.Clear();
    //        }
    //        if (currentStates.Count == 0)
    //        {
    //            returnValue.AppendLine("AllNonDeterministicFiniteAutomata Rejects.");
    //        }
    //        else
    //        {
    //            bool FullyAccepts = true;
    //            for (int i = 0; i < currentStates.Count; i++)
    //            {
    //                if (fa.AcceptStates.Contains(currentStates[i]))
    //                {
    //                    returnValue.AppendFormat("AllNonDeterministicFiniteAutomata Accepts: [{0}]\"{1}\"\r\n", i, currentStates[i]);

    //                }
    //                else
    //                {
    //                    returnValue.AppendFormat("AllNonDeterministicFiniteAutomata Rejects: [{0}]\"{1}\"\r\n", i, currentStates[i]);
    //                    FullyAccepts = false;    
    //                }
    //            }

    //            if (FullyAccepts)
    //            {
    //                returnValue.AppendLine("AllNonDeterministicFiniteAutomata FULLY Accepts the Input String.");
    //            }
    //            else
    //            {
    //                returnValue.AppendLine("AllNonDeterministicFiniteAutomata Rejects the Input String.");
    //            }
    //        }
    //        return returnValue.ToString();
            
    //    }
    //}
}
