﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Autonima
{
    public class InifiniteTape
    {
        public string ReadWrite
        {
            get
            {
                return this[Position];
            }
            set
            {
                this[Position] = value;
            }
        }

        public void MoveRight()
        {
            Position++;
        }

        public void MoveLeft()
        {
            Position--;
        }

        public string this[int x]
        {
            get
            {
                if (Tape.ContainsKey(x))
                {
                    return Tape[x];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (!Tape.ContainsKey(x))
                {
                    Tape.Add(x, value);
                }
                else
                {
                    Tape[x] = value;
                }

                if (value == null)
                {
                    Tape.Remove(x);
                }
            }
        }

        Dictionary<int, string> Tape = new Dictionary<int, string>();
        int Position = 0;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            List<int> items = new List<int>(Tape.Keys);
            items.Sort();
            sb.AppendLine("InifiniteTape");
            sb.AppendLine("Position:" + Position);
            sb.AppendLine("{");
            foreach (int i in items)
            {
                sb.AppendFormat("\t[{1}]=>\"{2}\"\r\n", i, Tape[i]);
            }
            sb.AppendLine("}");
            return sb.ToString();
        }

        private static Regex parse = new Regex("^\t\\[(?<position>[0-9]+)\\]=>\"(?<value>.+)\"$",RegexOptions.Compiled);

        public static InifiniteTape Read(StreamReader sr)
        {
            InifiniteTape it = new InifiniteTape();
            string temp = sr.ReadLine();
            if (temp.CompareTo("InifiniteTape") != 0)
                throw new Exception("Invalid Identifier: " + temp);
            temp = sr.ReadLine();
            if (!temp.StartsWith("Position:"))
                throw new Exception("Invalid Position identifier.");
            it.Position = int.Parse(temp.Replace("Position:", ""));

            temp = sr.ReadLine();
            if (temp.CompareTo("}") != 0)
                throw new Exception("Unexpected Token: " + temp);

            do
            {
                temp = sr.ReadLine();
                Match m = parse.Match(temp);
                int location = int.Parse(m.Groups["position"].Value);
                it[location] = m.Groups["value"].Value;

            } while ((temp.CompareTo("}") != 0) && !sr.EndOfStream);


            if (temp.CompareTo("}") != 0)
                throw new Exception("Unexpected Token: " + temp);

            return it;
        }

        public static void Write(InifiniteTape it, StreamWriter sw)
        {
            sw.Write(it.ToString());
        }

        private InifiniteTape()
        {
        }
    }

    class TuringMachines
    {
        private TuringMachines()
        {
        }

        public static void Write(TuringMachines tm, StreamWriter sw)
        {

        }


        public static TuringMachines Read(StreamReader sr)
        {
            TuringMachines tm = new TuringMachines();

            return tm;
        }
    }
}
