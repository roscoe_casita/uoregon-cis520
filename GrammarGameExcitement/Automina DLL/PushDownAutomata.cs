﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Autonima
{
    // this represents each line item / transition in a Push Down Automata.
    public class PDATransition
    {
        // we only create this privately, 
        private PDATransition(string source_state,string input_match, string stack_match, string stack_action,string destination_state)
        {
            // from state
            SourceState = source_state;
            // on the input
            InputMatch = input_match;
            // with the top of the stack equal to
            StackMatch = stack_match;

            //the list of stack actions to take (both the 'listed' and the 'reverse' ones we actually need to do.
            StackActions = new List<string>();
            ReverseStackActions = new List<string>();


            // if the stack is complex, then split it up.
            if (stack_action.Contains(StackActionsDelimiter))
            {
                StackActions.AddRange(stack_action.Split(StackActionsDelimiters, StringSplitOptions.RemoveEmptyEntries));
                ReverseStackActions.AddRange(StackActions);
                // we need to reverse this, because the stack is backwards.
                ReverseStackActions.Reverse();
            }
            else
            {
                if (stack_action.CompareTo("") != 0)
                {
                    StackActions.Add(stack_action);
                    ReverseStackActions.Add(stack_action);
                }
            }
            DestinationState = destination_state;
        }

        /// <summary>
        ///  start state.
        /// </summary>
        public readonly string SourceState;
        /// <summary>
        /// what is the input we match.
        /// </summary>
        public readonly string InputMatch;
        /// <summary>
        /// what is on the stack, to match.
        /// </summary>
        public readonly string StackMatch;
        /// <summary>
        /// what do we do to the stack, on a match.
        /// </summary>
        public readonly List<string> StackActions;

        /// <summary>
        /// real actions to do the stack.
        /// </summary>
        public readonly List<string> ReverseStackActions;

        /// <summary>
        ///  new state to transition to. 
        /// </summary>
        public readonly string DestinationState;


        /// <summary>
        /// Restore a PDATransition from a file.
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        public static PDATransition Read(StreamReader sr)
        {


            string temp = sr.ReadLine();


            // we could do stronger validation... but this is a pretty decent start anyways.
            
            if((temp.Contains(StartStateDelimiter)==false) ||
                (temp.Contains(InputStackDelimiter) == false) ||
                (temp.Contains(StackActionDelimiter) == false) ||
                (temp.Contains(ActionDestinationDelimiter) == false))
            {
                throw new Exception("Incorrectly formated PDA Transition function table Entry: " + temp);
            }

            string [] temps = temp.Split(Delimiters,StringSplitOptions.None);

            // and as long as we have the required 5 and only 5 values, then we will call it 'good enough'
            if (temps.Length != 5)
            {
                throw new Exception("Incorrect count of items for PDA Transition function table Entry: " + temp);
            }

            return new PDATransition(temps[0], temps[1], temps[2], temps[3],temps[4]);
        }

        /// <summary>
        /// save a PDATransition to a file.
        /// </summary>
        /// <param name="pdat"></param>
        /// <param name="sw"></param>
        public static void Write(PDATransition pdat, StreamWriter sw)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0}:{1},{2}->",pdat.SourceState,pdat.InputMatch,pdat.StackMatch);

            foreach(string s in pdat.StackActions)
            {
                sb.AppendFormat("{0}.",s);
            }
            sb.Remove(sb.Length-1,1);
            sb.AppendFormat(";{0}",pdat.DestinationState);

            sw.WriteLine(sb.ToString());
        }

        // some helpful strings to delimit
        private static string [] Delimiters = {":",",","->",";"};
        private const string StartStateDelimiter = ":";
        private const string InputStackDelimiter = ",";
        private const string StackActionDelimiter = "->";
        private const string ActionDestinationDelimiter = ";";

        private static string[] StackActionsDelimiters = { "." };
        private const string StackActionsDelimiter = ".";

        // escape sequence for pretty format
        private static string Epsiloned(string s)
        {
            if (s.CompareTo("") == 0)
                return "\u03B5";
            else
                return s;

        }


        // write the entire transition out.
        public override string  ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0},{1}->",Epsiloned(InputMatch) ,Epsiloned(StackMatch) );

            if (StackActions.Count > 0)
            {
                foreach (string s in StackActions)
                {
                    sb.AppendFormat("{0}.", s);
                }
                sb.Remove(sb.Length - 1, 1);
            }
            else
            {
                sb.Append(Epsiloned(""));
            }
            return sb.ToString();
        }
    }

    /// <summary>
    /// this will process the pushdownautomata rules.
    /// </summary>
    public class PushDownAutomata
    {
        // not part of the formal 6 tuple, but its very nice for printing prettily.
        public string Name = "";

        // the start state
        public string StartState = "";
        // the list of states.
        public List<string> States = new List<string>();
        // the input alphabet.
        public List<string> InputAlphabet = new List<string>();
        // the stack alphabet.
        public List<string> StackAlphabet = new List<string>();
        // the accept states.
        public List<string> AcceptStates = new List<string>();

        // the transitions from state to state.
        public Dictionary<string, List<PDATransition>> TransitionFunction = new Dictionary<string, List<PDATransition>>();

        // the one and only pda parser.
        private PushDownAutomataParser TypeParser = null;

        private static string[] splitter = { ":" };
        private static string state_delimiter = ":";
        private int DepthCounter = 0;

        // how many rules to we have.. .actually kind of useless.
        public int GetRuleCount() { return DepthCounter; }

        
        // here we will read a PDA from file.
        public static PushDownAutomata Read(StreamReader sr)
        {
            PushDownAutomata returnValue = new PushDownAutomata();
            string TypeName = sr.ReadLine();

            switch (TypeName)
            {
                //case "DeterministicPushDownAutomata":
                //    returnValue.TypeParser = new DeterministicPushDownAutomataParser();
                //    break;
                case "NonDeterministicPushDownAutomata":
                    returnValue.TypeParser = new NonDeterministicPushDownAutomataParser();
                    break;
                default:
                    throw new Exception("Wrong Type of Push Down Automata Encountered: " + TypeName);
            }
            //name of the FA
            returnValue.Name = sr.ReadLine();
            //the starting state to being from.
            returnValue.StartState = sr.ReadLine();
            // the list of all states.
            returnValue.States = sr.ReadLine().Split(splitter, StringSplitOptions.RemoveEmptyEntries).ToList();
            // the alphabet of input strings.
            returnValue.InputAlphabet = sr.ReadLine().Split(splitter, StringSplitOptions.None).ToList();
            // the alphabet of stack strings.
            returnValue.StackAlphabet = sr.ReadLine().Split(splitter, StringSplitOptions.None).ToList();
            // the list of states that we can finish in.
            returnValue.AcceptStates = sr.ReadLine().Split(splitter, StringSplitOptions.RemoveEmptyEntries).ToList();

            // the transition table.
            while (!sr.EndOfStream)
            {
                PDATransition transition = PDATransition.Read(sr);

                if (returnValue.TransitionFunction.ContainsKey(transition.SourceState) == false)
                {
                    returnValue.TransitionFunction.Add(transition.SourceState,new List<PDATransition>());
                }
                returnValue.TransitionFunction[transition.SourceState].Add(transition);
                returnValue.DepthCounter++;
            }

            // now we do some simple validation:
            // can't have a non-existant start state.
            if (!returnValue.States.Contains(returnValue.StartState))
            {
                throw new Exception("States Does not contain the Start State: " + returnValue.StartState);

            }
            foreach (string state in returnValue.AcceptStates)
            {

                // can't have accept states that don't exist.
                if (!returnValue.States.Contains(state))
                {
                    throw new Exception("States Does Not Contain the Accpet State: " + state);
                }
            }
            foreach (List<PDATransition> pdats in returnValue.TransitionFunction.Values)
            {
                foreach (PDATransition pdat in pdats)
                {
                    // all states must exist.
                    if (!returnValue.States.Contains(pdat.SourceState))
                        throw new Exception("States Does Not Contain the state: " + pdat.SourceState);

                    // all states must exist.
                    if (!returnValue.States.Contains(pdat.DestinationState))
                        throw new Exception("States Does Not Contain the state: " + pdat.DestinationState);

                    // all input alphabets must match.
                    if (!returnValue.InputAlphabet.Contains(pdat.InputMatch))
                        throw new Exception("Input Alphabet Does Not Contain the string: " + pdat.InputMatch);

                    // all stack alphabets must match,
                    if (!returnValue.StackAlphabet.Contains(pdat.StackMatch))
                        throw new Exception("Stack Alphabet Does Not Contain the string: " + pdat.StackMatch);
                    foreach (string s in pdat.StackActions)
                    {
                        if (!returnValue.StackAlphabet.Contains(s))
                            throw new Exception("Stack Alphabet Does not Contain the Stack Action: " + s);

                    }
                }
            }
            // call the type parser validator.
            returnValue.TypeParser.Validate(returnValue);

            return returnValue;
        }

        public static void Write(PushDownAutomata pda, StreamWriter sw)
        {
            // write the type of PDA this is (NDPDA or DPDA)
            sw.WriteLine(pda.TypeParser.TypeName);
            // write the name, this is not usually part of the "Formal 6 tuple", but it sure is nice to have.
            sw.WriteLine(pda.Name);
            // write the start state.
            sw.WriteLine(pda.StartState);
            // write the list of states.
            sw.WriteLine(BuildStringList(pda.States));
            // the alphabet of input strings.
            sw.WriteLine(BuildStringList(pda.InputAlphabet));
            // alphabet of stack states.
            sw.WriteLine(BuildStringList(pda.StackAlphabet));
            // accepting states.
            sw.WriteLine(BuildStringList(pda.AcceptStates));


            // transition function. its big, so its more broken out now.
            foreach (List<PDATransition> pdats in pda.TransitionFunction.Values)
            {
                foreach (PDATransition pdat in pdats)
                {
                    PDATransition.Write(pdat, sw);
                }
            }
            
        }

        // helper function to write all the strings out.
        private static string BuildStringList(IEnumerable<string> strings)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in strings)
            {
                sb.Append(s);
                sb.Append(state_delimiter);
            }
            sb.Remove(sb.Length - state_delimiter.Length, state_delimiter.Length);
            return sb.ToString();
        }

        // generate a pretty picture with this.
        static GraphVizWrapper.IGraphGeneration Igraph;

        // generate a the byte stream.
        public static byte[] GetImage(PushDownAutomata pda)
        {

            if (Igraph == null)
            {
                GraphVizWrapper.Queries.GetProcessStartInfoQuery gpsiq = new GraphVizWrapper.Queries.GetProcessStartInfoQuery();
                GraphVizWrapper.Queries.GetStartProcessQuery gspq = new GraphVizWrapper.Queries.GetStartProcessQuery();
                GraphVizWrapper.Commands.RegisterLayoutPluginCommand rlpc = new GraphVizWrapper.Commands.RegisterLayoutPluginCommand(gpsiq, gspq);
                Igraph = new GraphVizWrapper.GraphGeneration(gspq, gpsiq, rlpc);

            }
            string graph = BuildGraphVizDisplay(pda);
            return Igraph.GenerateGraph(graph, GraphVizWrapper.Enums.GraphReturnType.Jpg);
        }

        // here we generate the graph of the PDA
        public static String BuildGraphVizDisplay(PushDownAutomata pda)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("digraph \"" + pda.Name + "\"{");

            sb.AppendLine("graph[fontname = \"Helvetica-Oblique\",");
            sb.AppendLine("fontsize = 12,");
            sb.AppendLine("label = \"" + pda.Name + "," + DateTime.Now.ToShortDateString() + "\",");
            sb.AppendLine("size = \"12,12\"];");
            sb.AppendLine("secret_node [style=invis];");
            foreach (string state in pda.States)
            {
                string shape = "circle";

                if (pda.AcceptStates.Contains(state))
                {
                    shape = "doublecircle";
                }
                sb.AppendFormat("{0} [label=\"{1}\",shape={2}];\r\n", state.Replace(" ", ""), state, shape);
            }

            // graph viz needs this double encoded new line.
            string newline = "\\n";

            foreach (KeyValuePair<string, List<PDATransition>> state in pda.TransitionFunction)
            {
                Dictionary<string, StringBuilder> StateTransitions = new Dictionary<string, StringBuilder>();

                foreach (PDATransition pdat in state.Value)
                {
                    if (!StateTransitions.ContainsKey(pdat.DestinationState))
                    {
                        StateTransitions.Add(pdat.DestinationState, new StringBuilder());
                    }
                    StateTransitions[pdat.DestinationState].AppendFormat("{0}{1}", pdat.ToString(),newline);
                }
                foreach (KeyValuePair<string, StringBuilder> t in StateTransitions)
                {
                    t.Value.Remove(t.Value.Length - newline.Length, newline.Length);
                    sb.AppendFormat("{0} -> {1} [label=\"{2}\"];\r\n", state.Key.Replace(" ", ""), t.Key.Replace(" ", ""), t.Value.ToString());
                }
            }
            sb.AppendLine("secret_node -> " + pda.StartState.Replace(" ", "") + "[label=\"start\"];");
            sb.AppendLine("}");
            return sb.ToString();
        }

        private PushDownAutomata()
        {
        }

        // parse a string, dump to the output, is it in the language.
        public static bool ParseString(PushDownAutomata PDA, string text, out string output)
        {
            List<string> inputs = new List<string>();
            if (text.Contains(state_delimiter))
            {
                inputs.AddRange(text.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
            }
            else
            {
                foreach (char c in text)
                {
                    string s1 = "" + c;
                    inputs.Add(s1);
                }
            }

            return PDA.TypeParser.Parse(PDA,inputs,out output);
        
        }
    }

    public interface PushDownAutomataParser
    {
        string TypeName { get; }
        bool Parse(PushDownAutomata fa, List<string> inputs, out string output_text);
        bool Validate(PushDownAutomata fa);
    }

    // helper class to keep track of the multiple machines running. 
    public class Computation:Stack<string>
    {
        // branch a computation at the current point.
        public Computation Branch()
        {

            Computation returnValue = new Computation();

            // copy the stack
            foreach (string s in this.Reverse())
            {
                returnValue.Push(s);
            }
            // copy the current input position, state, and the transitions taken.
            returnValue.CurrentState = CurrentState;
            returnValue.Position = Position;
            returnValue.Transitions.AddRange(this.Transitions);
            return returnValue;
        }


        // enumerate the computation path.
        public IEnumerable<PDATransition> GetTransitions() { return Transitions; }


        private List<PDATransition> Transitions = new List<PDATransition>();
        public string CurrentState = null;
        public int Position = 0;

        // escape strings.
        private static string Epsiloned(string s)
        {
            if (s.CompareTo("") == 0)
                return "\u03B5";
            else
                return s;

        }

        // validate the action, and take it.
        public bool ValidAction(PDATransition transition,List<string> Input,StringBuilder sb)
        {
            if (transition.SourceState != CurrentState)
            {
                return false;
                throw new Exception("INVALID SOURCE STATE FOR TRANSITION!");
            }

            // not an Epsilon , and the input doesn't match, we can't make the transition.
            if (transition.InputMatch.CompareTo("") != 0)
            {
                if (this.Position >= Input.Count)
                {
                    return false;
                }
                else if(transition.InputMatch.CompareTo(Input[this.Position])==0)
                {
                    Position++;
                }
                else
                {
                    return false;
                }
            }

            // if its not an epsilon stack match, and the stack doesn't match, we can't make the transition.
            if ((transition.StackMatch.CompareTo("") != 0) &&
                (this.Count > 0) &&
                (transition.StackMatch.CompareTo(Peek())!=0))
            {
                return false;
            }

            // write to the output log
            sb.AppendFormat("\"{0}\" : [{1}].{2} + {3} -> \"{4}\"\r\n", transition.SourceState, Epsiloned(transition.InputMatch), Epsiloned(transition.StackMatch), ToString(), transition.DestinationState);

            // pop off the stack.
            if (transition.StackMatch.CompareTo("") != 0)
            {
                Pop();
            }

            // push actions on the stack.
            foreach (string s in transition.ReverseStackActions)
            {
                Push(s);
            }

            // transition state.
            CurrentState = transition.DestinationState;

            // add the transition to the list.
            Transitions.Add(transition);
            return true;
        }

        // write the stack computation out.
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Stack:(");
            if (this.Count > 0)
            {
                foreach (string s in this)
                {
                    sb.Append(s + ",");
                }
                if (sb.Length > 1)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
            }
            else
            {
                sb.Append(Epsiloned(""));
            }
            sb.Append(")");
            return sb.ToString();
        }
    }

    public class NonDeterministicPushDownAutomataParser : PushDownAutomataParser
    {
        // escape epsilons
        private static string Epsiloned(string s)
        {
            if (s.CompareTo("") == 0)
                return "\u03B5";
            else
                return s;

        }

        // this.Type.Name
        public string TypeName
        {
            get { return "NonDeterministicPushDownAutomata"; }
        }

        //nothing to special.
        public bool Validate(PushDownAutomata fa)
        {
            return true;
        }


        // parse the Input, with the given PDA, into the output processor, and tell us if it is/is not in the language.
        public bool Parse(PushDownAutomata PDA,List<string> input,  out string output_text)
        {
            StringBuilder sb = new StringBuilder();

            // 
            foreach(string s in input)
            {
                if(!PDA.InputAlphabet.Contains(s))
                {
                    output_text = "Invalid Input Alphabet String Item: " + s;
                    return false;
                }
            }
            Computation test_comp = new Computation();
            test_comp.CurrentState = PDA.StartState;
            /// here we go. we will need some extra classes for this one.
            List<Computation> Computations = new List<Computation>();

            List<Computation> AcceptingComputations = new List<Computation>();
            //Computations.Add(first_comp);
            Computations.Add(test_comp);
            List<Computation> new_computations = new List<Computation>();
            List<Computation> swap = null;

            bool Done = input.Count < 1;

            // alright, we will not search any deeper then this... although we will never even hit this because its REALLY deeeep.
            int MaxCount = PDA.GetRuleCount() + input.Count;

            if (input.Count == test_comp.Position)
            {
                if (PDA.AcceptStates.Contains(test_comp.CurrentState))
                {
                    AcceptingComputations.Add(test_comp);
                }
                else
                {
                    test_comp = null;
                }
                // we have found our computation path that accepts.
            }
            else
            {
                test_comp = null;
            }

            // alright begin.
                     
            while (!Done)
            {

                test_comp = null;
                
                // Non-Deterministic... aka, foreach computation...
                foreach (Computation comp in Computations)
                {  
                    // otherwise, are we in an accepting state?
                    if (PDA.AcceptStates.Contains(comp.CurrentState))
                    {
                        // and are we done with the input & the stack is empty?
                        if( (input.Count == comp.Position) &&  (comp.Count == 0))
                        {
                            // we have found our computation path that accepts.
                            AcceptingComputations.Add(comp);
                        }
                    }
                    // if we can transition from the current state.
                    else if (PDA.TransitionFunction.ContainsKey(comp.CurrentState))
                    {
                        // then for each transition that is possible.
                        foreach (PDATransition pdat in PDA.TransitionFunction[comp.CurrentState])
                        {
                            // branch,
                            Computation c = comp.Branch();

                            // if its valid
                            if (c.ValidAction(pdat, input, sb))
                            {
                                // add it to the next set.
                                new_computations.Add(c);
                            }

                        }
                    }
                    else
                    {
                        sb.AppendLine("Removing Dead Computation: " + comp.ToString());
                    }
                }
                // one pass done.
                sb.AppendLine("-------------------------------------------");                            

                swap = Computations;
                Computations = new_computations;
                new_computations = swap;
                new_computations.Clear();

                if ((Computations.Count == 0) || (MaxCount < 1))
                {
                    Done = true;
                }
                MaxCount--;
            }

            bool returnValue = false;

            if (AcceptingComputations.Count > 0)
            {
                sb.AppendLine("Total Accepting Computation Paths: " + AcceptingComputations.Count);
                returnValue = true;
                foreach (Computation comp in AcceptingComputations)
                {
                    StringBuilder sb2 = new StringBuilder("Accept String:");
                    sb.AppendLine("----------------------------------------------");
                    sb.AppendLine(" Accepting Computation Path:");
                    sb.AppendLine("----------------------------------------------");
                    foreach (PDATransition transition in comp.GetTransitions())
                    {
                        if (transition.InputMatch.CompareTo("") != 0)
                        {
                            sb2.Append(transition.InputMatch + ":");
                        }
                        sb.AppendFormat("\"{0}\" : [{1}] + {{", transition.SourceState, Epsiloned(transition.InputMatch));
                        if (transition.StackActions.Count > 0)
                        {
                            foreach (String s in transition.StackActions)
                            {
                                sb.Append(s);
                                sb.Append(",");
                            }
                            sb.Remove(sb.Length - 1, 1);
                        }
                        else
                        {
                            sb.Append(Epsiloned(""));
                        }
                        sb.AppendFormat("}} -> \"{0}\"\r\n", transition.DestinationState);
                    }
                    if(sb2.Length>0)
                    {
                        sb2.Remove(sb2.Length - 1, 1);
                    }
                    sb.AppendLine("----------------------------------------------");

                    sb.AppendLine("PushDownAutomata Accepts Computation Path.");
                    sb.AppendLine(sb2.ToString());
                }

            }
            else
            {
                sb.AppendLine("PushDownAutomata Rejects All Computation Paths.");
            }
            output_text = sb.ToString();
            return returnValue;
        }
    }

    //public class DeterministicPushDownAutomataParser : PushDownAutomataParser
    //{

    //    public string TypeName
    //    {
    //        get { return "DeterministicPushDownAutomata"; }
    //    }

    //    public bool Parse(PushDownAutomata fa, List<string> inputs, out string output_text)
    //    {
    //        bool returnValue = false;
    //        StringBuilder sb = new StringBuilder();



    //        output_text = sb.ToString();
    //        return returnValue;
    //    }

    //    public bool Validate(PushDownAutomata fa)
    //    {
    //        bool returnValue = true;

    //        if(fa.InputAlphabet.Contains(""))
    //        {
    //            throw new Exception("Input Alphabet Contains Epsilon Transitions.");
    //        }
    //        if(fa.StackAlphabet.Contains(""))
    //        {
    //            throw new Exception("Stack Alphabet Contains Epsilon Transitions.");
    //        }
    //        foreach(


    //        return returnValue;
    //    }
    //}

}
